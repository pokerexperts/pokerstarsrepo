﻿// Class: Card.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Used to represent each card in the game.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROTPS3
{
    /// <summary>
    /// The 4 possible suits of cards in the game.
    /// </summary>
    public enum  Suit
    {
        Diamonds=1,
        Clubs,
        Spades,
        Hearts

    }

    /// <summary>
    /// This class represents each card in the game.
    /// </summary>
    class Card
    {
        /// <summary>
        /// The value of this card.
        /// </summary>
        private int _value;

        /// <summary>
        /// The suit of this card.
        /// </summary>
        private Suit _suit;

        /// <summary>
        /// The value of this card.
        /// </summary>
        public int Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
            }
        }

        /// <summary>
        /// The suit of this card.
        /// </summary>
        public Suit Suit
        {
            get
            {
                return _suit;
            }

            set
            {
                _suit = value;
            }
        }

        /// <summary>
        /// Generates and sets the string for the image location of this card.
        /// </summary>
        public string GetImageLocation()
        {
            string suitString = "";

            // Determine the letter of the suit for the location string, based on the card's suit.
            if (_suit == Suit.Clubs)
            {
                suitString = "c";
            }
            else if (_suit == Suit.Diamonds)
            {
                suitString = "d";
            }
            else if (_suit == Suit.Hearts)
            {
                suitString = "h";
            }
            else if (_suit == Suit.Spades)
            {
                suitString = "s";
            }
            else
            {
                Debug.Assert(false, "Invalid suit.");
            }

            // Determine the letter of the value for the location string, based on the card's value.
            // A zero will need to be added in front if the value is below 10.
            // If the value is 14, the string will be 01. (Aces.)
            string valueString = "";

            if (_value < 10)
            {
                valueString = "0" + _value.ToString();
            }
            else if (_value == 14)
            {
                valueString = "01";
            }
            else
            {
                valueString = _value.ToString();
            }

            // Return the completed string for the location of this card's image file.
            return ("ms-appx:///Assets/" + suitString + valueString + ".png");
        }
    }
}
