﻿// Class: Bet.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Used to store each player's current personal bet.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROTPS3
{
    /// <summary>
    /// A bet that a player has made.
    /// </summary>
    class Bet
    {
        /// <summary>
        /// The amount of money being waged in this bet.
        /// </summary>
        private int _amount;

        /// <summary>
        /// Initializes the amount for this bet. 
        /// </summary>
        /// <param name="amount"> The amount of money being waged in this bet. </param>
        public Bet(int amount)
        {
            _amount = amount;
        }

        /// <summary>
        /// The amount of money that has been bet by the player.
        /// </summary>
        public int Amount
        {
            set { _amount = value; }
            get { return _amount; }
        }
    }
}
