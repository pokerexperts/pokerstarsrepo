﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace ROTPS3
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamePage : Page
    {
        /// <summary>
        /// The game object containing the logical layer of the application.
        /// </summary>
        private Game _game;

        /// <summary>
        /// Constructor for the GamePage. Creates a game object with passed in settings from the
        /// settings page.
        /// </summary>
        public GamePage()
        {
            this.InitializeComponent();

            // Initializes as null. The true game object will be passed from the settings page.
            _game = null;
        }

        /// <summary>
        /// When the deal button is pressed, the GamePage will tell the game to deal out some cards
        /// depending on what point in the game it is.
        /// </summary>
        /// <param name="sender"> The deal button. </param>
        /// <param name="e"> The arguments related to the deal button. </param>
        private void OnDeal(object sender, RoutedEventArgs e)
        {
            bool endOfRound = false;
            // Tells the game to deal out cards, whether that be to players or the table.
            // Also used to start a new round.
            _game.Deal(out endOfRound);

            // If a round has just ended, the deal button will stay enabled.
            if (endOfRound == false)
            {
                // Enables the other three buttons, while disabling the deal button.
                _btnCall.IsEnabled = true;
                _btnFold.IsEnabled = true;
                _btnRaise.IsEnabled = true;
                _btnDeal.IsEnabled = false;
                // Enable raise money textbox.
                _txtBet.IsEnabled = true;
            }

            // Check if the game is over. If it is, navigate to the highscore page.
            if (_game.GameOver)
            {
                Frame.Navigate(typeof(ScorePage), _game);
            }
        }

        /// <summary>
        /// When the raise button is pressed, the amount typed into the bet box will be checked for an
        /// integer. If the entry is valid, the game will be told how much the player would like to 
        /// raise the current bet.
        /// </summary>
        /// <param name="sender"> The raise button. </param>
        /// <param name="e"> The arguments related to the raise button. </param>
        private void OnRaise(object sender, RoutedEventArgs e)
        {
            // Clear record textbox from previous bets.
            _txtRecord.Text = "";

            // Tells the game to raise to a certain amount. (2 is the raise option.)
            try
            {
                int amount = int.Parse(_txtBet.Text);
                bool bettingPhaseComplete = false;
                _game.BettingPhase(2, out bettingPhaseComplete, amount);

                // If the betting phase is complete, enable dealing.
                if (bettingPhaseComplete == true)
                {
                    // Enables deal, while disabling other buttons.
                    _btnCall.IsEnabled = false;
                    _btnFold.IsEnabled = false;
                    _btnRaise.IsEnabled = false;
                    _btnDeal.IsEnabled = true;
                    // Disable raise money textbox.
                    _txtBet.IsEnabled = false;
                }

            }
            catch(OverflowException)
            {
                // Do nothing. The number entered was too large.
                return;
            }
            catch(FormatException)
            {
                // Do nothing. A non-integer was entered.
                return;
            }
        }

        /// <summary>
        /// When the call button is pressed, the player has decided to simply meet the current bet.
        /// Tell the game to make the player bet the current amount.
        /// </summary>
        /// <param name="sender"> The call button. </param>
        /// <param name="e"> The arguments related to the call button. </param>
        private void OnCall(object sender, RoutedEventArgs e)
        {
            // Clear record textbox from previous bets.
            _txtRecord.Text = "";

            // Tells the game that the player would like to match the current bet. (1 is the call option.)
            bool bettingPhaseComplete = false;
            _game.BettingPhase(1, out bettingPhaseComplete);

            // If the betting phase is complete, enable dealing.
            if (bettingPhaseComplete == true)
            {
                // Enables deal, while disabling other buttons.
                _btnCall.IsEnabled = false;
                _btnFold.IsEnabled = false;
                _btnRaise.IsEnabled = false;
                _btnDeal.IsEnabled = true;
                // Disable raise money textbox.
                _txtBet.IsEnabled = false;
            }
        }

        /// <summary>
        /// When the fold button is pressed, the player has decided to drop out of the round.
        /// Tell the game to remove the player from the round's betting and winnings.
        /// </summary>
        /// <param name="sender"> The fold button. </param>
        /// <param name="e"> The arguments related to the fold button. </param>
        private void OnFold(object sender, RoutedEventArgs e)
        {
            // Clear record textbox from previous bets.
            _txtRecord.Text = "";

            // Tells the game that the player would like to fold. (3 is the fold option.)
            // Play out the remaining part of the game, making the user fold every time.
            bool bettingPhaseComplete = false;
            bool endOfRound = false;
            while (!endOfRound)
            {
                _game.BettingPhase(3, out bettingPhaseComplete);
                _game.Deal(out endOfRound);
            }
            
            // Enables deal, while disabling other buttons.
            _btnCall.IsEnabled = false;
            _btnFold.IsEnabled = false;
            _btnRaise.IsEnabled = false;
            _btnDeal.IsEnabled = true;
            // Disable raise money textbox.
            _txtBet.IsEnabled = false;

        }

        /// <summary>
        /// Takes the game object from the settings page when a transition to playing happens.
        /// Sets the Ui controls for the _game to control.
        /// </summary>
        /// <param name="e"> The game object containing the logic layer of the application. </param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _game = e.Parameter as Game;

            // Set the minimum bet box to show the current minimum.
            _txtMinBet.Text = "Min. Bet: $" + _game.MinBet;

            // Create a list of the 8 image controls for the players' hands, and set the controls.
            List<Image> listOfPlayerImageControls = new List<Image>();
            listOfPlayerImageControls.Add(_imgP1C1);
            listOfPlayerImageControls.Add(_imgP1C2);
            listOfPlayerImageControls.Add(_imgP2C1);
            listOfPlayerImageControls.Add(_imgP2C2);
            listOfPlayerImageControls.Add(_imgP3C1);
            listOfPlayerImageControls.Add(_imgP3C2);
            listOfPlayerImageControls.Add(_imgP4C1);
            listOfPlayerImageControls.Add(_imgP4C2);

            // Create a list of the TextBlocks used by players to display their money.
            List<TextBlock> listOfMoneyDisplays = new List<TextBlock>();
            listOfMoneyDisplays.Add(_txtPlay1Money);
            listOfMoneyDisplays.Add(_txtPlay2Money);
            listOfMoneyDisplays.Add(_txtPlay3Money);
            listOfMoneyDisplays.Add(_txtPlay4Money);

            // Pass through the lists of image controls and money displays to set on the players.
            _game.SetPlayerDisplays(listOfPlayerImageControls, listOfMoneyDisplays);

            // Create the table for the game, passing in the 5 revealed cards' image controls.
            _game.Table = new Table(_imgT1, _imgT2, _imgT3, _imgT4, _imgT5);

            // Assign the record UI TextBlock to the _game.
            _game.RecordUi = _txtRecord;

            // Assign the pool UI TextBlock to the _game.
            _game.PoolUi = _txtPool;

            // Assign the current bet UI TextBlock to the _game.
            _game.CurrentBetUi = _txtCurrentBet;

            // Set player names on displays.
            _txtPlay1Name.Text = _game.PlayerList[0].Name;
            _txtPlay2Name.Text = _game.PlayerList[1].Name;
            _txtPlay3Name.Text = _game.PlayerList[2].Name;
            _txtPlay4Name.Text = _game.PlayerList[3].Name;
        }
    }
}
