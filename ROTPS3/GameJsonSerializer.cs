﻿// Class: GameJsonSerializer.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Used to store game file in JSON between each round of the game for resuming later.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Popups;

namespace ROTPS3
{
    /// <summary>
    /// Saves the game object using JSON, so that the game can be resumed after any turn in the game.
    /// </summary>
    class GameJsonSerializer
    {
        /// <summary>
        /// The game object to be saved for resuming the game.
        /// </summary>
        protected Game _game;

        /// <summary>
        /// The game save directory.
        /// </summary>
        protected string _gameSavePath;

        /// <summary>
        /// Constructor for the game's serializer. Initializes the game as null, to be changed later as a
        /// property. Also creates directory to hold the file.
        /// </summary>
        public GameJsonSerializer()
        {
            // Initialize game as null to be set using a property later.
            _game = null;

            // Initialize the save path for the game object.
            _gameSavePath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "GameSaveFolder");

            // Create a directory for the game file if it does not exist.
            if (Directory.Exists(_gameSavePath) == false)
            {
                Directory.CreateDirectory(_gameSavePath);
            }
        }

        /// <summary>
        /// The game object to be saved for resuming the game.
        /// </summary>
        public Game Game
        {
            get { return _game; }
            set { _game = value; }
        }

        /// <summary>
        /// Loads the game file and resumes, if the file exists.
        /// </summary>
        public void Load()
        {
            // Determine file save path.
            string gameSaveFilePath = Path.Combine(_gameSavePath, "GameAutoSaveFile");

            // Check if file exists. If it does, load it.
            if (File.Exists(gameSaveFilePath))
            {
                // Load the game object.
                using (FileStream gameStream = new FileStream(gameSaveFilePath, FileMode.Open))
                {
                    // Deserialize the game object.
                    DataContractJsonSerializer gameSerializer = new DataContractJsonSerializer(typeof(Game));

                    // Store the game object.
                    _game = gameSerializer.ReadObject(gameStream) as Game;

                    // Loaded successfully. Tell the user an old game file was loaded.
                    MessageDialog msgDlg = new MessageDialog("An old game save was found! The game has now been loaded. Press play to continue it.", "Resuming...");
                    msgDlg.ShowAsync();
                }
            }
        }

        /// <summary>
        /// Saves the game file to be resumed if the game is exited at the end of a round.
        /// </summary>
        public void Save()
        {
            // Determine file save path.
            string gameSaveFilePath = Path.Combine(_gameSavePath, "GameAutoSaveFile");

            // Save or overwrite with current game object.
            using (FileStream gameStream = new FileStream(gameSaveFilePath, FileMode.Create))
            {
                // Serialize the game object.
                DataContractJsonSerializer gameSerializer = new DataContractJsonSerializer(typeof(Game));

                gameSerializer.WriteObject(gameStream, _game);
            }
        }

        /// <summary>
        /// Deletes an old game save file so that it cannot be resumed after the game is completed.
        /// </summary>
        public void DeleteSave()
        {
            // Determine file save path.
            string gameSaveFilePath = Path.Combine(_gameSavePath, "GameAutoSaveFile");

            // Check if file exists. If it does, delete it.
            if (File.Exists(gameSaveFilePath))
            {
                File.Delete(gameSaveFilePath);
            }
        }
    }
}
