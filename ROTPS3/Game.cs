﻿// Class: Game.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// The core class of the application's business logic. This runs the game of poker, from start to finish.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace ROTPS3
{
    /// <summary>
    /// This class represents the logic behind the game itself.
    /// </summary>
    [DataContract]
    class Game
    {
        /// <summary>
        /// The amount of money in the current pool.
        /// </summary>
        private int _pool;

        /// <summary>
        /// The amount of money required as a minimum bet for the players.
        /// </summary>
        [DataMember (Name = "MinBet")]
        private int _minBet;

        /// <summary>
        /// The score of the user for the highscore list.
        /// </summary>
        private int _userScore;

        /// <summary>
        /// The number of the current round.
        /// </summary>
        [DataMember(Name = "RoundNum")]
        private int _roundNumber;

        /// <summary>
        /// The number of times the deal button has been pressed in a given round.
        /// </summary>
        private int _dealNum;

        /// <summary>
        /// The current amount of money as the set bet.
        /// </summary>
        private int _currentBet;

        /// <summary>
        /// The TextBlock Ui used for records of what happens in the game.
        /// </summary>
        private TextBlock _recordUi;

        /// <summary>
        /// The TextBlock Ui used for diplaying the current bet.
        /// </summary>
        private TextBlock _currentBetUi;

        /// <summary>
        /// The list of players playing in the game. (The user, plus the three AIs.)
        /// </summary>
        [DataMember(Name = "Players")]
        private List<Player> _listOfPlayers;

        /// <summary>
        /// The list of cards on the table.
        /// </summary>
        private Table _table;

        /// <summary>
        /// The list of cards in the deck.
        /// </summary>
        private Deck _deck;

        /// <summary>
        /// The list of high scores from past playthroughs.
        /// </summary>
        [DataMember(Name = "Highscores")]
        private List<Score> _listOfHighscores;

        /// <summary>
        /// The TextBlock that displays the amount of money currently in the pool.
        /// </summary>
        private TextBlock _poolUi;

        /// <summary>
        /// The amount of money each player starts with in the game.
        /// </summary>
        [DataMember(Name = "StartAmount")]
        private int _startingAmount;

        /// <summary>
        /// When this is true, the game has ended and it is time to navigate to the highscore screen.
        /// </summary>
        private bool _gameOver;

        /// <summary>
        /// The manager of the Ais and their settings.
        /// </summary>
        //[DataMember(Name = "AiManager")] (TODO: Set ai manager to have data serialization?)
        private AIManager _aiManager;

        /// <summary>
        /// The serializer for saving the highscore list as a text file.
        /// </summary>
        private HighscoreListTextSerializer _highscoreTextSerializer;

        /// <summary>
        /// The serializer for saving the game object as a JSON file for resuming later.
        /// </summary>
        private GameJsonSerializer _gameJsonSerializer;

        /// <summary>
        /// The constructor of the game class. This will initialize the game, setting up the
        /// cards, players, and empty AI list.
        /// </summary>
        public Game()
        {
            // Initialize all values to defaults.
            _pool = 0;
            _minBet = 5;
            _userScore = 0;
            _roundNumber = 0;
            _dealNum = 0;
            _currentBet = 0;
            _recordUi = null;
            _poolUi = null;
            _currentBetUi = null;
            _startingAmount = 1000;
            _gameOver = false;
            _highscoreTextSerializer = new HighscoreListTextSerializer();
            _gameJsonSerializer = new GameJsonSerializer();

            // Create an empty list of players to play the game. 
            _listOfPlayers = new List<Player>();

            // Populate list of players.
            CreatePlayers();

            // Create a table to hold the revealed cards. (Equal to null until set when game page
            // is navigated to, to set the image controls.)
            _table = null;

            // Create a deck of cards for the game.
            _deck = new Deck();

            // Initialize empty highscore list.
            _listOfHighscores = new List<Score>();

            // Fill highscore list with default highscores, or load from file.
            LoadScores();

            // Initialize the Ai manager.
            _aiManager = new AIManager(this);


        }

        /// <summary>
        /// The amount of money any given player starts with in the game.
        /// </summary>
        public int StartingMoney
        {
            get { return _startingAmount; }
            set { _startingAmount = value; }
        }

        /// <summary>
        /// The TextBlock used to display the current amount of money in the pool.
        /// </summary>
        public TextBlock PoolUi
        {
            set { _poolUi = value; }
        }

        /// <summary>
        /// The TextBlock used to display the current bet.
        /// </summary>
        public TextBlock CurrentBetUi
        {
            set { _currentBetUi = value; }
        }

        /// <summary>
        /// The table used to hold the revealed cards.
        /// </summary>
        public Table Table
        {
            get { return _table; }
            set { _table = value; }
        }

        /// <summary>
        /// When this is true, the game is over.
        /// </summary>
        public bool GameOver
        {
            get { return _gameOver; }
            set { _gameOver = value; }
        }

        /// <summary>
        /// The minimum bet allowed each round of this game.
        /// </summary>
        public int MinBet
        {
            get { return _minBet; }
            set { _minBet = value; }
        }

        /// <summary>
        /// The Textblock used to record events in the game and display them to the user.
        /// When the game page is created, the Ui will be set so that the game can change it.
        /// </summary>
        public TextBlock RecordUi
        {
            set { _recordUi = value; }
        }

        /// <summary>
        /// The finalized list of highscores from the game.
        /// </summary>
        public List<Score> HighscoreList
        {
            get { return _listOfHighscores; }
        }

        /// <summary>
        /// The list of players playing the game.
        /// </summary>
        public List<Player> PlayerList
        {
            get { return _listOfPlayers; }
            set { _listOfPlayers = value; }
        }

        /// <summary>
        /// The number of deals that have happened in the round so far.
        /// </summary>
        public int DealNum
        {
            get { return _dealNum; }
        }

        /// <summary>
        /// The number of rounds that have taken place in a game.
        /// </summary>
        public int RoundNum
        {
            get { return _roundNumber; }
        }

        /// <summary>
        /// The serializer used to save the game between rounds for resuming later.
        /// </summary>
        public GameJsonSerializer GameJsonSerializer
        {
            get { return _gameJsonSerializer; }
            set { _gameJsonSerializer = value; }
        }

        /// <summary>
        /// Creates the default four players for the game and populates the list of players.
        /// </summary>
        public void CreatePlayers()
        {
            // Create a list of default player names.
            List<string> defaultPlayerNames = new List<string>();
            defaultPlayerNames.Add("Magdin");
            defaultPlayerNames.Add("Carl");
            defaultPlayerNames.Add("Xavier");
            defaultPlayerNames.Add("Jimmy");

            // Create four players. Assign their numbers, names, and set the default start money to $1000.
            // Then, add each player to the list of players.
            for (int playerNum=1; playerNum < 5; playerNum++)
            {   
                // Create player.
                // Find names in the defaultPlayerNames list, by index, from 0 to 3.
                string playerName = defaultPlayerNames[playerNum-1];

                // Create the player.
                Player newPlayer = new Player(playerName, playerNum);

                // Give starting money to player.
                newPlayer.MoneyChange(_startingAmount);

                // Add player to list.
                _listOfPlayers.Add(newPlayer);
            }
        }

        /// <summary>
        /// Sets the image controls and money displays for each players when game page is navigated to.
        /// </summary>
        /// <param name="listOfPlayerImageControls"> The list of the 8 image controls used by the players
        /// to display their hands on the screen. </param>
        /// <param name="listOfMoneyDisplays"> The list of the four displaying TextBlocks to show player money. </param>
        public void SetPlayerDisplays(List<Image> listOfPlayerImageControls, List<TextBlock> listOfMoneyDisplays)
        {
            foreach (Player player in _listOfPlayers)
            {
                // Find the image controls in the list passed in as a parameter, listOfPlayerImageControls.
                // Use the player number to determine which 2 controls need to be used from the list for this
                // player.
                player.FirstCard = listOfPlayerImageControls[(player.PlayerNum * 2) - 2];
                player.SecondCard = listOfPlayerImageControls[(player.PlayerNum * 2) - 1];

                // Set the TextBox used to display this player's money.
                player.MoneyDisplay = listOfMoneyDisplays[player.PlayerNum - 1];

                // Set the money display to show the proper value.
                player.MoneyDisplay.Text = "$" + player.Money.ToString();
            }
        }

        /// <summary>
        /// Checks for a highscore file, loads it if it exists. Otherwise makes default scores.
        /// </summary>
        public void LoadScores()
        {
            // Search for a highscore save file. If it exists, load it.
            _highscoreTextSerializer.Load();
            _listOfHighscores = _highscoreTextSerializer.HighscoreList;

            // If the score list is still empty, no file was found.
            if (_listOfHighscores.Count == 0)
            {
                // If no file is found, create the default highscores.
                CreateDefaultScores();
            }
        }

        /// <summary>
        /// Saves the highscores of the game to a file when a full game finishes, adding
        /// user's score if worthy enough.
        /// </summary>
        public void SaveScores()
        {
            // Add the user's score to the highscore list, then remove the lowest score.
            // Set the _recentUserScore of this score to true, to represent it being recently done by the user.
            Score newScore = new Score(_listOfPlayers[0].Name, _userScore);
            newScore.RecentUserScore = true;
            _listOfHighscores.Add(newScore);
            foreach (Score score1 in _listOfHighscores)
            {
                int counter = 0;
                foreach (Score score2 in _listOfHighscores)
                {
                    if (score1.ScoreValue <= score2.ScoreValue)
                    {
                        counter++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (counter == 11)
                {
                    _listOfHighscores.Remove(score1);
                    break;
                }
            }

            // When highscore list is ready, save it to a text file. (Set the serializer highscore list and tell it
            // to save.)         
            _highscoreTextSerializer.HighscoreList = _listOfHighscores;
            _highscoreTextSerializer.Save();
            
        }

        /// <summary>
        /// Called when the deal button is pressed. Checks which deal in the round this is, and
        /// does an appropriate action. Could be dealing cards to players, or dealing to the table.
        /// Can also retrieve cards for the next round.
        /// </summary>
        public void Deal(out bool endOfRound)
        {
            // Increment number of deals in the round by 1.
            _dealNum++;

            // Each time deal is pressed, clear the record.
            _recordUi.Text = "";

            // Default 'dealToContinue' to false, but if its the end of the round, make it true.
            endOfRound = false;

            // If this is the first deal, give cards out to each player.
            if (_dealNum == 1)
            {
                DealHands();
            }

            // If this is the second deal, reveal the first 3 cards off of the deck.
            else if (_dealNum == 2)
            {
                // First reveal of cards, hence the 1.
                RevealCards(1);
            }

            // If this is the third deal, reveal the fourth card off of the deck.
            else if (_dealNum == 3)
            {
                RevealCards(2);
            }

            // If this is the fourth and last deal, reveal the fifth card off of the deck.
            else if (_dealNum == 4)
            {
                RevealCards(3);
            }

            // If this is the fifth deal, end the round.
            else if (_dealNum == 5)
            {
                // End round.
                EndOfRound();
                // Keep deal button enabled.
                endOfRound = true;
            }

            // Retrieve cards and clean up for new round. Set _dealNum back to 0.
            else
            {
                RetrieveCards();
                _dealNum = 0;
                // Keep deal button enabled.
                endOfRound = true;
            }
        }

        /// <summary>
        /// This method is called in order to reveal cards on the table.
        /// </summary>
        /// <param name="revealNum"> The number of reveals that have taken place after this call. </param>
        private void RevealCards(int revealNum)
        {
            // If this is the first reveal, reveal three cards on the table from the deck.
            if (revealNum == 1)
            {
                Card drawnCard = _deck.DrawCard();
                _table.RevealedCardList.Add(drawnCard);
                _table.FirstCard.Source = new BitmapImage(new Uri(drawnCard.GetImageLocation()));

                drawnCard = _deck.DrawCard();
                _table.RevealedCardList.Add(drawnCard);
                _table.SecondCard.Source = new BitmapImage(new Uri(drawnCard.GetImageLocation()));

                drawnCard = _deck.DrawCard();
                _table.RevealedCardList.Add(drawnCard);
                _table.ThirdCard.Source = new BitmapImage(new Uri(drawnCard.GetImageLocation()));
            }
            // If this is the second reveal, reveal the fourth card from the deck.
            else if (revealNum == 2)
            {
                Card drawnCard = _deck.DrawCard();
                _table.RevealedCardList.Add(drawnCard);
                _table.FourthCard.Source = new BitmapImage(new Uri(drawnCard.GetImageLocation()));
            }
            // If this is the third reveal, reveal the last card from the deck.
            else if (revealNum == 3)
            {
                Card drawnCard = _deck.DrawCard();
                _table.RevealedCardList.Add(drawnCard);
                _table.FifthCard.Source = new BitmapImage(new Uri(drawnCard.GetImageLocation()));
            }
            // Error code.
            else
            {
                Debug.Assert(false, "Error, invalid revealNum.");
            }
        }

        /// <summary>
        /// Deals out hands to each player at the table and displays them appropriately.
        /// </summary>
        private void DealHands()
        {
            // Increment the round number by one, for calculating highscores.
            _roundNumber++;

            // Give each player at the table a hand of cards. (If they are still in the game.)
            foreach(Player player in _listOfPlayers)
            {
                if (player.Eliminated == false)
                {
                    // Draw two cards from the deck and add them to the player's hand.
                    player.Hand.HandList.Add(_deck.DrawCard());
                    player.Hand.HandList.Add(_deck.DrawCard());

                    // Tell the player to display their hand. (Face-up only if user.)
                    // Set roundOver as false. This is the beginning of the round.
                    player.DisplayHand(false);

                    // Deduct the minimum bet from this player's money and add to the pool.
                    int amountBet = player.MoneyChange(-_minBet);
                    _pool += amountBet;

                    // Show change to pool.
                    _poolUi.Text = "Pool: $" + _pool.ToString();
                }

            }
        }

        /// <summary>
        /// Retrieves cards from the table and players to shuffle back into the deck.
        /// </summary>
        public void RetrieveCards()
        {
            // Take all player and table cards and put them back into the deck.

            // Add all cards from the table to the deck.
            _deck.DeckList.AddRange(_table.RevealedCardList);

            // Clear the table's card list.
            _table.RevealedCardList.Clear();

            // Show that the cards are off of the table.
            _table.FirstCard.Source = new BitmapImage(new Uri("ms-appx:///Assets/emptycard.png"));
            _table.SecondCard.Source = new BitmapImage(new Uri("ms-appx:///Assets/emptycard.png"));
            _table.ThirdCard.Source = new BitmapImage(new Uri("ms-appx:///Assets/emptycard.png"));
            _table.FourthCard.Source = new BitmapImage(new Uri("ms-appx:///Assets/emptycard.png"));
            _table.FifthCard.Source = new BitmapImage(new Uri("ms-appx:///Assets/emptycard.png"));

            foreach (Player player in _listOfPlayers)
            {
                // Add the cards from the players' hand to the deck.
                _deck.DeckList.AddRange(player.Hand.HandList);

                // Clear the hand.
                player.Hand.HandList.Clear();

                // Show that the cards have been removed.
                player.FirstCard.Source = new BitmapImage(new Uri("ms-appx:///Assets/emptycard.png"));
                player.SecondCard.Source = new BitmapImage(new Uri("ms-appx:///Assets/emptycard.png"));
            }
            // Shuffle the deck.
            _deck.ShuffleCards();
        }

        /// <summary>
        /// Creates the default highscores if no highscore save file is found.
        /// </summary>
        public void CreateDefaultScores()
        {
            // Create 10 highscore objects with the default values, and add each to the list.
            _listOfHighscores.Add(new Score("Magdin", 9999));
            _listOfHighscores.Add(new Score("Washington", 9000));
            _listOfHighscores.Add(new Score("Jimmy the 3rd", 7564));
            _listOfHighscores.Add(new Score("Cell Phone Lizard", 6223));
            _listOfHighscores.Add(new Score("Augustus Ceasar", 5467));
            _listOfHighscores.Add(new Score("A Rabbit", 3010));
            _listOfHighscores.Add(new Score("Xavier", 2998));
            _listOfHighscores.Add(new Score("Carl", 2004));
            _listOfHighscores.Add(new Score("Monkey King", 1546));
            _listOfHighscores.Add(new Score("Carl's Brother", 1232));
        }

        /// <summary>
        /// Determines the score a player gets at the end of a game according to how fast they
        /// completed the game with a win. (If they lose, score is 0.)
        /// </summary>
        /// <param name="userWin"> Whether the user won or lost. True if they won. </param>
        public void DetermineScore(bool userWin)
        {
            if (userWin == true)
            {
                // Determine score as 10000, minus 400 for each round that was needed to win.
                _userScore = 10000 - (_roundNumber * 400);
            }

            else
            {
                // Score is 0. The player lost.
                _userScore = 0;
            }
        }

        /// <summary>
        /// This method will be called in order to complete a betting phase before the next deal.
        /// </summary>
        public void BettingPhase(int userChoice, out bool bettingPhaseComplete, int userRaiseBy = 0)
        {
            // Call PlayerAction with 1 as the player number. (User player's action.)
            PlayerAction(userChoice, 1, userRaiseBy);

            // Tell the Ais to complete their turns, by calling on PlayerAction().
            OtherPlayActions();

            // Once everyone has taken their action, the game will check to see if the current bet was
            // met by everyone at the table. If it was, the deal button will be enabled again.
            int playerBetCheck = 0;
            foreach(Player player in _listOfPlayers)
            {
                // Check if each player's bet value is equal to the current bet.
                // OR, if a player has no remaining money.
                if (_currentBet == player.Bet || player.Money == 0 || player.Folded)
                {
                    playerBetCheck++;
                }
                // If there are less than 3 AIs, an error has occured. The AIManager did not make enough AIs
                // to play out this game. They are not playing the turns of 1 or more of the AIs.
                else if (_aiManager.AI.Count < 3)
                {
                    _aiManager.AIActions();
                    //Debug.Assert(false, "Error. The AI Manager did not create enough AIs for the game.");
                }
            }
            // If playerBetCheck == 4, all players have met the current bet.
            if (playerBetCheck == 4)
            {
                // All players have met the current bet. Enable deal button.
                bettingPhaseComplete = true;
            }
            // If the bet was not met by everyone at the table, another phase of betting takes place
            // for the players who did not meet the current bet yet.
            else
            {
                // Not all players have met the current bet. Start a new betting phase for those who have
                // not.
                bettingPhaseComplete = false;
            }

        }

        /// <summary>
        /// This method is called when the player makes a choice on what to do during their turn.
        /// </summary>
        /// <param name="PlayerChoice"> What type of action the player would like to make during the turn.  </param>
        /// <param name="playerNumber"> The number of the player calling this method. </param>
        /// <param name="raiseBy"> If raising, the amount the player would like to raise by. </param>
        public void PlayerAction(int playerChoice, int playerNumber, int raiseBy = 0)
        {
            Player selectedPlayer = null;
            // Find the selected player.
            foreach(Player player in _listOfPlayers)
            {
                // Find the player with the matching player number.
                if (player.PlayerNum == playerNumber)
                {
                    selectedPlayer = player;
                    break;
                }
            }

            // Check if the player has folded. If they have, they will not be able to participate
            // in the rest of the round. (If they are eliminated, they don't get to play at all.)
            // Also check if they have no money. If they have no money, they will not play until
            // they have money again. (Say they are calling if they are in the round. This is for players who went 'all in'.)
            // (The record will only change if this is not the user.)
            if (selectedPlayer.Folded || selectedPlayer.Eliminated)
            {
                return;
            }
            else if (selectedPlayer.Money == 0 )
            {
                if (selectedPlayer.PlayerNum != 1)
                {
                    _recordUi.Text += selectedPlayer.Name + " calls.\n";
                }
                return;
            }

            // If playerChoice is 1, the player has chosen to call.
            if (playerChoice == 1)
            {
                // Make the change to the player's money and display it.
                int actualBetAmount = selectedPlayer.MoneyChange(-(_currentBet - selectedPlayer.Bet));

                // Add to the player's current bet value.
                selectedPlayer.MakeBet(actualBetAmount);

                // Set the current bet for the table and display it.
                _currentBet = selectedPlayer.Bet;
                _currentBetUi.Text = "Bet: $" + _currentBet;

                // Add to the pool whatever money was bet, and show this change.
                _pool += actualBetAmount;
                _poolUi.Text = "Pool: $" + _pool;

                // Record what happened if this is not the user.
                if (playerNumber != 1)
                {
                    _recordUi.Text += selectedPlayer.Name + " calls.\n";
                }
            }

            // If playerChoice is 2, the player has chosen to raise.
            else if (playerChoice == 2)
            {
                // Make the player meet the current bet.
                int callingAmount = selectedPlayer.MoneyChange(-_currentBet);
                // Then, they can try to raise.
                int actualRaiseAmount = selectedPlayer.MoneyChange(-raiseBy);

                // Add to the player's current bet value.
                selectedPlayer.MakeBet(actualRaiseAmount);

                // The amount was raised, change the current bet accordingly and show the change.
                _currentBet += actualRaiseAmount;
                _currentBetUi.Text = "Bet: $" + _currentBet;

                // Add to the pool whatever money was bet, and show this change.
                _pool += (actualRaiseAmount + callingAmount);
                _poolUi.Text = "Pool: $" + _pool;

                // Record what happened if this is not the user.
                if (playerNumber != 1)
                {
                    if (actualRaiseAmount != 0)
                    {
                        _recordUi.Text += selectedPlayer.Name + " raises by $" + actualRaiseAmount + ".\n";
                    }
                    else
                    {   // An AI is trying to raise by $0. Record that they are calling instead.
                        _recordUi.Text += selectedPlayer.Name + " calls.\n";
                    }
                }
            }

            // If playerChoice is 3, the player has chosen to fold.
            else if (playerChoice == 3)
            {
                // The player has folded, set their folded boolean to true.
                selectedPlayer.Folded = true;

                // Record what happened if this is not the user.
                if (playerNumber != 1)
                {
                    _recordUi.Text += selectedPlayer.Name + " folds.\n";
                }
            }

            // Error, if code reaches here.
            else
            {
                Debug.Assert(false, "Error, invalid playerChoice.");
            }
        }

        /// <summary>
        /// Use this method to tell each Ai to take their turns.
        /// </summary>
        public void OtherPlayActions()
        {
            // Call _aiManager.AIActions() to tell the AIs to take their turns.
            // They will complete their turns by each calling PlayerAction() with parameters to
            // specify what they would like to do.

            _aiManager.AIActions();
        }

        /// <summary>
        /// Called at the end of the round when all betting phases are finished.
        /// </summary>
        public void EndOfRound()
        {
            // Create an empty list of hand scores for the round.
            List<int> handScores = new List<int>();
            // All betting and revealing is finished. It is time to end the round.
            // Reveal all players' cards.
            foreach (Player player in _listOfPlayers)
            {
                // The round is over, so pass true in as roundOver parameter.
                player.DisplayHand(true);

                // Check if the player has folded or been eliminated. If they have, they will get a handscore of 0.
                if (player.Folded == true || player.Eliminated == true)
                {
                    handScores.Add(0);
                }
                else
                {
                    // Find this player's hand score and add to the list if they are in the round still.
                    handScores.Add(player.DetermineHandScore(_table.RevealedCardList));
                }
            }
            // Determine the winner(s) of the round based on hand scores.
            int winningScore = 0;
            foreach(int handScore1 in handScores)
            {
                int counter = 0;
                foreach(int handScore2 in handScores)
                {
                    if (handScore1 >= handScore2)
                    {
                        counter++;
                    }
                }
                // If the score is equal to or greater than all others, it is the winning score.
                if (counter == 4)
                {
                    winningScore = handScore1;
                    break;
                }
            }
            // Make a list of winning players.
            List<Player> winningPlayers = new List<Player>();
            for (int playerNumber = 0; playerNumber < 4; playerNumber++)
            {
                if (winningScore == handScores[playerNumber])
                {
                    winningPlayers.Add(_listOfPlayers[playerNumber]);
                }
            }
            // Give the pool money to the player(s) who won. (Divide it according to number of winners.)
            int dividedWinnings = _pool / winningPlayers.Count;
            foreach (Player player in winningPlayers)
            {
                _pool -= dividedWinnings;
                player.MoneyChange(dividedWinnings);
            }
            // Display the changes to the pool money.
            _poolUi.Text = "Pool: $" + _pool.ToString();

            // Set the current bet to 0.
            _currentBet = 0;
            _currentBetUi.Text = "Bet: $0";

            // If there is only one winner...
            if (winningPlayers.Count == 1)
            {
                // Announce in the record ui who won.
                _recordUi.Text = winningPlayers[0].Name + " wins with a " + winningPlayers[0].Hand.HandType + "!";
            }
            else
            {
                // Announce those who had a draw.
                if (winningPlayers.Count == 2)
                {
                    _recordUi.Text = winningPlayers[0].Name + " and " + winningPlayers[1].Name + " have tied.";
                }
                else if (winningPlayers.Count == 3)
                {
                    _recordUi.Text = winningPlayers[0].Name + ", " + winningPlayers[1].Name + " and " + winningPlayers[2].Name + " have tied.";
                }
                else
                {
                    _recordUi.Text = "Everyone wins!";
                }
            }

            // Set all Folded values to false, for the next round. Eliminate any players with $0.
            // Set all players' current bets to 0, as well.
            foreach(Player player in _listOfPlayers)
            {
                player.Bet = 0;

                player.Folded = false;

                if (player.Money == 0)
                {
                    player.Eliminated = true;
                }
            }

            // Check if all ais are eliminated.
            int aiEliminatedCounter = 0;
            foreach(Player player in _listOfPlayers)
            {
                if (player.PlayerNum != 1)
                {
                    if (player.Eliminated == true)
                    {
                        aiEliminatedCounter++;
                    }
                }
            }

            // Check if the game is over. If it is, call EndGame().
            if (_listOfPlayers[0].Money == 0 || aiEliminatedCounter == 3)
            {
                EndOfGame();
            }
            else
            {
                // If the game isn't over, save the game file to allow the next round to be played later if desired.
                _gameJsonSerializer.Game = this;
                _gameJsonSerializer.Save();
            }
        }

        /// <summary>
        /// Called when the game ends. (Player is eliminated or Ais have all lost.)
        /// </summary>
        public async void EndOfGame()
        {
            // Delete any game autosave files. The game is done and no longer can be resumed.
            _gameJsonSerializer.DeleteSave();

            if (_listOfPlayers[0].Money == 0)
            {
                // The player has lost the game. Display an appropriate message.
                MessageDialog msgDlg = new MessageDialog("You were eliminated. You lose!", "Game Over");
                await msgDlg.ShowAsync();

                // Determine their highscore. (False, meaning they lost.)
                DetermineScore(false);
            }
            else
            {
                // The player has won the game. Display an appropriate message.
                MessageDialog msgDlg = new MessageDialog("You have eliminated your foes. You win!", "Game Over");
                await msgDlg.ShowAsync();

                // Determine their highscore. (True, meaning they won.)
                DetermineScore(true);
            }

            _gameOver = true;

            // Check if new score needs to be added to list, then save highscores.
            SaveScores();
        }

        /// <summary>
        /// Resets game values for the game to be played again.
        /// </summary>
        public void Reset()
        {
            _gameOver = false;

            foreach(Player player in _listOfPlayers)
            {
                player.ResetVals(_startingAmount);
            }
        }

        /// <summary>
        /// Prepares a resumed game file for play. Completes the game initialization steps not saved in the file.
        /// </summary>
        public void PrepareForPlay()
        {
            // Initialize all unsaved values to defaults.
            _pool = 0;
            _userScore = 0;
            _dealNum = 0;
            _currentBet = 0;
            _recordUi = null;
            _poolUi = null;
            _currentBetUi = null;
            _gameOver = false;
            _highscoreTextSerializer = new HighscoreListTextSerializer();
            _gameJsonSerializer = new GameJsonSerializer();

            // Prepare the players to resume the game.
            foreach (Player player in _listOfPlayers)
            {
                player.PreparePlayer();
            }

            // Create a table to hold the revealed cards. (Equal to null until set when game page
            // is navigated to, to set the image controls.)
            _table = null;

            // Create a deck of cards for the game.
            _deck = new Deck();

            // Initialize the Ai manager.
            _aiManager = new AIManager(this);
        }
    }
}
