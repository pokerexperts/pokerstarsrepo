﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Windows.Storage;

namespace ROTPS3
{
    /// <summary>
    /// Saves the game object using XML, so that the settings page can restore the AIProfiles that the user made previously
    /// </summary>
    class AIProfileSerializerXML
    {
        /// <summary>
        /// The location where the  data will be stored
        /// </summary>
        private string _profileDirPath;
        /// <summary>
        /// list of all the AIProfiles's that was received from the files
        /// </summary>
        private List<AIProfiles> _aIProfileListing;
        /// <summary>
        /// constructor for the AIProfiles Serializer that make a folder if there is not one,also initializers it's list feild
        /// </summary>
        public AIProfileSerializerXML()
        {
            AIProfileListing = new List<AIProfiles>();
            //check and create the file folder where the data will be saved
            _profileDirPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "SettingsSaveFolder", "XML");
            if (Directory.Exists(_profileDirPath) == false)
            {
                Directory.CreateDirectory(_profileDirPath);
            }
        }
        /// <summary>
        /// get and set the list feild variable that is of type List<AIProfiles>
        /// </summary>
        public List<AIProfiles> AIProfileListing
        {
            get { return _aIProfileListing; }
            set { _aIProfileListing = value; }
        }
        /// <summary>
        /// get the feild variable that is of type string, it represents the path
        /// </summary>
        public string ProfileDirPath
        {
            get { return _profileDirPath; }
        }
        /// <summary>
        /// Loads the Settings files
        /// </summary>
        public void Load()
        {
            //get the list of files in the directory
            string[] acctFileList = Directory.GetFiles(ProfileDirPath);

            //go through the list of files, create the appropriate accounts and load the file
            foreach (string profileFileName in acctFileList)
            {
                using (FileStream acctStream = new FileStream(profileFileName, FileMode.Open))
                {
                    using (XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(acctStream,new XmlDictionaryReaderQuotas()))
                    {
                        //deserialize the account object
                        DataContractSerializer serializer = new DataContractSerializer(typeof(AIProfiles));
                        AIProfiles acct = serializer.ReadObject(reader, true) as AIProfiles;

                        //add the account to the list of accounts
                        AIProfileListing.Add(acct);
                    }
                }
            }
        }
        /// <summary>
        /// save the files in the Settings folder
        /// </summary>
        public void Save()
        {
            //go through each account in the list of accounts and ask it to save itself into a corresponding file
            foreach (AIProfiles iprofile in AIProfileListing)
            {
                //determine the account file name
                string filePath = $"{ProfileDirPath}/Acct{iprofile.Id}.xml";

                //write the account data to the file
                using (FileStream acctStream = new FileStream(filePath, FileMode.Create))
                {
                    //serialize the account object
                    DataContractSerializer serializer = new DataContractSerializer(typeof(AIProfiles));

                    serializer.WriteObject(acctStream, iprofile);
                }
            }
        }
    }
}
