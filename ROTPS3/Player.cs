﻿// Class: Player.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Used to store all of a player's information in the game, as well as allow them to bet and play.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace ROTPS3
{
    /// <summary>
    /// One of the four players playing in the game.
    /// </summary>
    [DataContract]
    class Player
    {
        /// <summary>
        /// The name of the player.
        /// </summary>
        [DataMember (Name = "Name")]
        private string _name;

        /// <summary>
        /// The amount of money the player currently has.
        /// </summary>
        [DataMember(Name = "Money")]
        private int _money;

        /// <summary>
        /// Whether the player has folded for this round. If true, they have.
        /// </summary>
        private bool _folded;

        /// <summary>
        /// Whether the player has been eliminated from the game for having no remaining money.
        /// If true, they have been eliminated.
        /// </summary>
        [DataMember(Name = "Eliminated")]
        private bool _eliminated;

        /// <summary>
        /// The number of the player. The user is 1, and the others, going clock-wise around the table,
        /// are 2, 3, and 4.
        /// </summary>
        [DataMember(Name = "PlayerNum")]
        private int _playerNum;

        /// <summary>
        /// The last bet that this player made.
        /// </summary>
        private Bet _bet;

        /// <summary>
        /// The 2-card hand that this player has currently.
        /// </summary>
        private Hand _hand;

        /// <summary>
        /// The image control for this player's first card on the page.
        /// </summary>
        private Image _imgFirstCard;

        /// <summary>
        /// The image control for this player's second card on the page.
        /// </summary>
        private Image _imgSecondCard;

        /// <summary>
        /// The money display for this player, on a TextBlock.
        /// </summary>
        private TextBlock _moneyDisplay;

        /// <summary>
        /// The constructor for a player. This will initialize the name, amount of money they have, _folded and
        /// _eliminated to false, and it will set their player number. Also initializes the hand to be
        /// empty and sets player image controls. (For hand.)
        /// </summary>
        /// <param name="name"> The name of the player being created. </param>
        /// <param name="playerNum"> The number of the player. The user is 1,
        /// and the others are created as 2, 3, and 4, clock-wise around the table from the user. </param>
        /// <param name="startingMoney"> The amount of money that this player starts with. </param>
        /// <param name="cardImage1"> The image control used to display the player's first card. </param>
        /// <param name="cardImage2"> The image control used to display the player's second card. </param>
        /// <param name="moneyDisplay"> The TextBlock used to show this player's current money. </param>
        public Player(string name, int playerNum)
        {
            // Set default values. Create empty hand and bet of 0.
            _name = name;
            _money = 0;
            _playerNum = playerNum;
            _folded = false;
            _eliminated = false;
            _bet = new Bet(0);
            _hand = new Hand();
            _imgFirstCard = null;
            _imgSecondCard = null;
            _moneyDisplay = null;
        }

        /// <summary>
        /// The name of the player.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// The TextBlock used to display this player's money.
        /// </summary>
        public TextBlock MoneyDisplay
        {
            get { return _moneyDisplay; }
            set { _moneyDisplay = value; }
        }

        /// <summary>
        /// The first card in the player's hand.
        /// </summary>
        public Image FirstCard
        {
            get { return _imgFirstCard; }
            set { _imgFirstCard = value; }
        }

        /// <summary>
        /// The second card in the player's hand.
        /// </summary>
        public Image SecondCard
        {
            get { return _imgSecondCard; }
            set { _imgSecondCard = value; }
        }

        /// <summary>
        /// The hand of this player.
        /// </summary>
        public Hand Hand
        {
            get { return _hand; }
            set { _hand = value; }
        }

        /// <summary>
        /// Whether the player has folded or not, this round. If true, they have.
        /// </summary>
        public bool Folded
        {
            get { return _folded; }
            set { _folded = value; }
        }

        /// <summary>
        /// Whether the player has been eliminated from the game yet. If true, they have been eliminated.
        /// </summary>
        public bool Eliminated
        {
            get { return _eliminated; }
            set { _eliminated = value; }
        }

        /// <summary>
        /// The number of the player. Starts at 1, being the user, then proceeds clock-wise around the table.
        /// (2, 3, and 4.)
        /// </summary>
        public int PlayerNum
        {
            get { return _playerNum; }
            set { _playerNum = value; }
        }

        /// <summary>
        /// The player's last bet amount.
        /// </summary>
        public int Bet
        {
            get { return _bet.Amount; }
            set { _bet.Amount = value; }
        }

        /// <summary>
        /// How much money this player has.
        /// </summary>
        public int Money
        {
            get { return _money; }
        }

        /// <summary>
        /// Adds this amount to their current bet for the round.
        /// </summary>
        /// <param name="amount"> The amount of money being waged in this bet. </param>
        public void MakeBet(int amount)
        {
            _bet.Amount += amount;

        }

        /// <summary>
        /// Determines the final score of a player's hand at the end of a round. Requires a list of table cards.
        /// </summary>
        /// <param name="revealedCardList"> The list of cards revealed on the table. </param>
        /// <returns> The determined score of the player's poker hand. </returns>
        public int DetermineHandScore(List<Card> revealedCardList)
        {
            // Ask the hand to determine the hand score with the table cards.
            _hand.HandScore(revealedCardList);
            // Return the hand score to the game.
            return _hand.FinalHandScore;
        }

        /// <summary>
        /// Displays the hand of this player on the board. If roundOver is true, all hands will be face up.
        /// </summary>
        /// <param name="roundOver"> True if the round is over and this is the final reveal. </param>
        public void DisplayHand(bool roundOver)
        {
            // If the round is not over, display cards in user's hand face up, others face-down.
            if (!roundOver)
            {
                // If _playerNum is 1, this is the user. Reveal their cards face up.
                if (_playerNum == 1)
                {
                    // Create strings representing the locations of each card image.
                    string playerCardImg1Location = _hand.HandList[0].GetImageLocation();
                    string playerCardImg2Location = _hand.HandList[1].GetImageLocation();

                    // Find appropriate images for the player's cards.
                    BitmapImage playerCardImg1 = new BitmapImage(new Uri(playerCardImg1Location));
                    BitmapImage playerCardImg2 = new BitmapImage(new Uri(playerCardImg2Location));

                    // Set the appropriate images to the image controls to show them.
                    _imgFirstCard.Source = playerCardImg1;
                    _imgSecondCard.Source = playerCardImg2;
                }
                // Otherwise this is an Ai. Place their cards face-down.
                else
                {
                    _imgFirstCard.Source = new BitmapImage(new Uri("ms-appx:///Assets/cardback.png"));
                    _imgSecondCard.Source = new BitmapImage(new Uri("ms-appx:///Assets/cardback.png"));
                }
            }
            // If the round is over, reveal all cards face up.
            else if (roundOver)
            {
                if (_eliminated == false)
                {
                    // Create strings representing the locations of each card image.
                    string playerCardImg1Location = _hand.HandList[0].GetImageLocation();
                    string playerCardImg2Location = _hand.HandList[1].GetImageLocation();

                    // Find appropriate images for the player's cards.
                    BitmapImage playerCardImg1 = new BitmapImage(new Uri(playerCardImg1Location));
                    BitmapImage playerCardImg2 = new BitmapImage(new Uri(playerCardImg2Location));

                    // Set the appropriate images to the image controls to show them.
                    _imgFirstCard.Source = playerCardImg1;
                    _imgSecondCard.Source = playerCardImg2;
                }
            }
        }

        /// <summary>
        /// Call this method to give or take money from a player.
        /// </summary>
        /// <param name="amount"> The amount of money for them to lose or gain. 
        /// Negative sign used for loss of money. </param>
        /// <returns> Returns the amount actually bet by the player. (0 if they aren't betting at this time.) </returns>
        public int MoneyChange(int amount)
        {
            // If amount is positive, the player is gaining money.
            if (amount > 0)
            {
                // Give the player their money.
                _money += amount;
                // Change the display to reflect the change. (If the money display exists yet.)
                if (_moneyDisplay != null)
                {
                    _moneyDisplay.Text = "$" + _money.ToString();
                }
                // Return 0; the player gained money.
                return 0;
            }

            // Otherwise, the player is betting.
            // Check if they can make the bet, and if not, they will use all their remaining money.
            else
            {
                // Check if they can make the bet.
                // If they can, return the amount designated.
                if (_money > -amount)
                {
                    int returnVal = -amount;
                    _money += amount;

                    // Display the new amount of money they have on screen.
                    _moneyDisplay.Text = "$" + _money.ToString();

                    return returnVal;
                }
                // If they can't, return all their remaining money.
                else
                {
                    int returnVal = _money;
                    _money = 0;

                    // Display the new amount of money they have on screen.
                    _moneyDisplay.Text = "$" + _money.ToString();

                    return returnVal;
                }
                
            }
        }

        /// <summary>
        /// Resets the _eliminated value to false and sets the player's money to the starting amount.
        /// </summary>
        /// <param name="StartingAmount"> The amount of money that players start the game with. </param>
        public void ResetVals(int StartingAmount)
        {
            _eliminated = false;
            _money = StartingAmount;
        }

        /// <summary>
        /// Prepares a player object for a resumed game, from earlier. Initializes things not saved.
        /// </summary>
        public void PreparePlayer()
        {
            // Set unsaved defaults.
            _folded = false;
            _bet = new Bet(0);
            _hand = new Hand();
            _imgFirstCard = null;
            _imgSecondCard = null;
            _moneyDisplay = null;
        }
    }
}
