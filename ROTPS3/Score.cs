﻿// Class: Score.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Used to store highscores for each player.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ROTPS3
{
    /// <summary>
    /// A highscore from a winning playthrough of the game.
    /// </summary>
    [DataContract]
    class Score
    {
        /// <summary>
        /// The name of the player who earned this score.
        /// </summary>
        [DataMember (Name = "Name")]
        private string _name;

        /// <summary>
        /// The score the player achieved.
        /// </summary>
        [DataMember (Name = "Score")]
        private int _score;

        /// <summary>
        /// The string to be printed on the highscore screen for this score.
        /// </summary>
        [DataMember (Name = "ScoreString")]
        private string _scoreString;

        /// <summary>
        /// If the score was made recently by a user, this will be set to true.
        /// </summary>
        private bool _recentUserScore;

        /// <summary>
        /// The score constructor. Takes in parameters for its name and score before producing and 
        /// storing a score string.
        /// </summary>
        /// <param name="name"> The name of the player who earned this score. </param>
        /// <param name="score"> The actual score earned. </param>
        public Score(string name, int score)
        {
            // Initialize name and score from parameters.
            _name = name;
            _score = score;

            // Set _recentUserScore to false on default. (Set later when score is created for user.)
            _recentUserScore = false;

            // Generate a string to be used at the highscore page.
            _scoreString = _name + " " + _score.ToString();

        }

        /// <summary>
        /// The name of the user who scored this highscore.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// The score value for this highscore entry.
        /// </summary>
        public int ScoreValue
        {
            get { return _score; }
        }

        /// <summary>
        /// The string printed on the highscore list when this score is displayed.
        /// </summary>
        public string ScoreString
        {
            get { return _scoreString; }
        }

        /// <summary>
        /// If this is true, the user earned this score in the most recent game played.
        /// </summary>
        public bool RecentUserScore
        {
            get { return _recentUserScore; }
            set { _recentUserScore = value; }
        }
    }
}
