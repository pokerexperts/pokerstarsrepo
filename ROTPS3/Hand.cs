﻿// Class: Hand.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Used to store the cards held by a player and evaluate hands in the game to determine winners.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROTPS3
{

    /// <summary>
    /// A two-card hand list possessed by one player. Poker hands will be ranked and given a score here.
    /// </summary>
    class Hand
    {
        /// <summary>
        /// The list of cards (2 cards.) in this player's hand.
        /// </summary>
        private List<Card> _handList;

        /// <summary>
        /// The final score of this hand, determined at the end of rounds.
        /// </summary>
        private int _finalHandScore;

        /// <summary>
        /// Used for telling the user what type of hand was used to win the round.
        /// This is the type of hand determined. (Pair, Full House, etc...)
        /// </summary>
        private string _handType;

        /// <summary>
        /// Initializes an empty hand for the player to put cards into.
        /// </summary>
        public Hand()
        {
            // Initialize empty hand for player.
            _handList = new List<Card>();

            // Initialize defaults.
            _finalHandScore = 0;
            _handType = "";
        }

        /// <summary>
        /// The two cards in a player's hand.
        /// </summary>
        public List<Card> HandList
        {
            get { return _handList; }
            set { _handList = value; }
        }

        /// <summary>
        /// The final determined score of this hand at the end of a round.
        /// </summary>
        public int FinalHandScore
        {
            get { return _finalHandScore; }
        }

        /// <summary>
        /// The type of hand this hand of cards was determined to be.
        /// </summary>
        public string HandType
        {
            get { return _handType; }
        }

        /// <summary>
        /// Determines the score of this player's hand using the 5 revealed cards on the table.
        /// By comparing different player scores for their hands, one can determine who wins a round.
        /// </summary>
        /// <param name="revealedCardList"> The list of 5 revealed cards, passed from the table. </param>
        public void HandScore(List<Card> revealedCardList)
        {
            // Reset any past hand scores from old rounds.
            _finalHandScore = 0;

            // Create a temporary list to hold a total of seven cards. 5 from the table, the other two from the player.
            List<Card> cardsAvailable = new List<Card>();

            // Add the cards from the table and hand to the available list.
            foreach (Card card in _handList)
            {
                cardsAvailable.Add(card);
            }
            foreach (Card card in revealedCardList)
            {
                cardsAvailable.Add(card);
            }

            // If _finalHandScore is still 0, a score has not been determined yet. Each of the following functions
            // will check the available cards to see if they match with a specific poker hand. If they do, 
            // a score will be given to the hand.

            RoyalFlush(cardsAvailable);

            if (_finalHandScore == 0)
            {
                StraightFlush(cardsAvailable);
            }
            if (_finalHandScore == 0)
            {
                FourOfAKind(cardsAvailable);
            }
            if (_finalHandScore == 0)
            {
                FullHouse(cardsAvailable);
            }
            if (_finalHandScore == 0)
            {
                Flush(cardsAvailable);
            }
            if (_finalHandScore == 0)
            {
                Straight(cardsAvailable);
            }
            if (_finalHandScore == 0)
            {
                ThreeOfAKind(cardsAvailable);
            }
            if (_finalHandScore == 0)
            {
                TwoPair(cardsAvailable);
            }
            if (_finalHandScore == 0)
            {
                Pair(cardsAvailable);
            }
            if (_finalHandScore == 0)
            {
                HighCard(cardsAvailable);
            }
        }

        /// <summary>
        /// Checks what the score of a high card hand is.
        /// </summary>
        private void HighCard(List<Card> cardsAvailable)
        {
            // Add each card value to a list of numbers.
            List<int> listOfCardNums = new List<int>();
            foreach (Card card in cardsAvailable)
            {
                listOfCardNums.Add(card.Value);
            }
            // For each card available for use in the hand, check if it is higher than the other four.
            foreach (int num1 in listOfCardNums)
            {
                int counter = 0;

                // Count for each number, how many numbers is it equal to or higher than in the list.
                foreach (int num2 in listOfCardNums)
                {
                    if (num1 >= num2)
                    {
                        counter++;
                    }
                }

                // If the number is higher than or equal to all cards in the list, it is the highest number.
                if (counter == 7)
                {
                    // This is the highest card available to this hand. Give it an appropriate score.
                    _finalHandScore = num1;
                    // Set the hand type string for the record.
                    _handType = "high card";
                }
            }
        }

        /// <summary>
        /// Checks if a hand is a pair. If so, determines score.
        /// </summary>
        private void Pair(List<Card> cardsAvailable)
        {
            // Create a list of card numbers available.
            List<int> listOfNums = new List<int>();
            foreach (Card card in cardsAvailable)
            {
                listOfNums.Add(card.Value);
            }

            // Send the list of card numbers to a function to determine if a pair is present.
            int commonNum = CheckIfSameNums(listOfNums, 2);

            // If there is a common number that is non-zero, there is a pair. Award 2000 points.
            // Add on the value of the pair to break ties.
            if (commonNum != 0)
            {
                _finalHandScore = 2000 + commonNum;
                // Set the hand type string for the record.
                _handType = "pair";
            }
        }

        /// <summary>
        /// Checks if a hand is a two pair. If so, determines score.
        /// </summary>
        private void TwoPair(List<Card> cardsAvailable)
        {
            // Create a list of card numbers available.
            List<int> listOfNums = new List<int>();
            foreach (Card card in cardsAvailable)
            {
                listOfNums.Add(card.Value);
            }

            // Send the list of card numbers to a function to determine if a pair is present.
            int commonNum1 = CheckIfSameNums(listOfNums, 2);
            // If a pair is present, remove all of that common number from the list of numbers.
            if (commonNum1 != 0)
            {
                foreach (int num in listOfNums.ToList())
                {
                    if (num == commonNum1)
                    {
                        listOfNums.Remove(num);
                    }
                }
                // Now, search the list for a pair.
                int commonNum2 = CheckIfSameNums(listOfNums, 2);
                // If another pair is also found, there are two pairs. Award 3000 points.
                // Add the values of both pairs to break ties.
                if (commonNum2 != 0)
                {
                    _finalHandScore = 3000 + commonNum1 + commonNum2;
                    // Set the hand type string for the record.
                    _handType = "two pair";
                }
            }
        }

        /// <summary>
        /// Checks if a hand is a three of a kind. If so, determines score.
        /// </summary>
        private void ThreeOfAKind(List<Card> cardsAvailable)
        {
            // Create a list of card numbers available.
            List<int> listOfNums = new List<int>();
            foreach (Card card in cardsAvailable)
            {
                listOfNums.Add(card.Value);
            }

            // Send the list of card numbers to a function to determine if a three of a kind is present.
            int commonNum = CheckIfSameNums(listOfNums, 3);

            // If a common number exists, this is a three of a kind. Reward 4000 points.
            // Add on the value of the set to break ties.
            if (commonNum != 0)
            {
                _finalHandScore = 4000 + commonNum;
                // Set the hand type string for the record.
                _handType = "three of a kind";
            }
        }

        /// <summary>
        /// Checks if a hand is a straight. If so, determines score.
        /// </summary>
        private void Straight(List<Card> cardsAvailable)
        {
            // Put the numbers of all the cards into a list.
            List<int> listOfCardNums = new List<int>();
            foreach (Card card in cardsAvailable)
            {
                listOfCardNums.Add(card.Value);
            }
            // Feed the card numbers into a function to determine if they contain five consecutive numbers.
            int straight = CheckIfConsecNums(listOfCardNums);

            // If there are 5 consecutive numbers, this is a straight. Award 5000 points.
            // Add on the value of the highest card in the hand of 5 cards to break ties.
            if (straight != 0)
            {
                _finalHandScore = 5000 + straight;
                // Set the hand type string for the record.
                _handType = "straight";
            }
        }

        /// <summary>
        /// Checks if a hand is a flush. If so, determines score.
        /// </summary>
        private void Flush(List<Card> cardsAvailable)
        {
            // Put the cards available into a function to determine if there is a common suit of 5 cards.
            string commonSuit = CheckIfCommonSuit(cardsAvailable);

            // Make a list of card numbers from that suit.
            List<int> listOfCardNums = new List<int>();
            foreach(Card card in cardsAvailable)
            {
                if ((card.Suit == Suit.Clubs && commonSuit == "clubs") || (card.Suit == Suit.Diamonds && commonSuit == "diamonds") || (card.Suit == Suit.Spades && commonSuit == "spades") || (card.Suit == Suit.Hearts && commonSuit == "hearts"))
                {
                    listOfCardNums.Add(card.Value);
                }
            }

            // Find the sum of the card values in that common suit.
            int sum = 0;
            foreach(int num in listOfCardNums)
            {
                sum += num;
            }

            // If there is a common suit of 5 cards at minimum, this is a flush. Award 6000 points.
            // Add on the sum of the cards in that suit. (This will be used to break ties.)
            if (commonSuit != "noncommon")
            {
                _finalHandScore = 6000 + sum;
                // Set the hand type string for the record.
                _handType = "flush";
            }
        }

        /// <summary>
        /// Checks if a hand is a full house. If so, determines score.
        /// </summary>
        private void FullHouse(List<Card> cardsAvailable)
        {
            // Create a list of card numbers available.
            List<int> listOfNums = new List<int>();
            foreach (Card card in cardsAvailable)
            {
                listOfNums.Add(card.Value);
            }

            // Send the list of card numbers to a function to determine if a three of a kind is present.
            int commonNum1 = CheckIfSameNums(listOfNums, 3);
            // If a three of a kind is present, remove all of that common number from the list of numbers.
            if (commonNum1 != 0)
            {
                foreach(int num in listOfNums.ToList())
                {
                    if (num == commonNum1)
                    {
                        listOfNums.Remove(num);
                    }
                }
                // Now, search the list for a pair.
                int commonNum2 = CheckIfSameNums(listOfNums, 2);
                // If a pair is also found, this is a full house. Award 7000 points.
                // Add the number of the three of a kind multiplied by 20, and the value of the pair.
                if (commonNum2 != 0)
                {
                    _finalHandScore = 7000 + (commonNum1 * 20) + commonNum2;
                    // Set the hand type string for the record.
                    _handType = "full house";
                }
            }
        }

        /// <summary>
        /// Checks if a hand is a four of a kind. If so, determines score.
        /// </summary>
        private void FourOfAKind(List<Card> cardsAvailable)
        {
            // Put every card in the available cards into a list of numbers.
            List<int> listOfCardNums = new List<int>();
            foreach (Card card in cardsAvailable)
            {
                listOfCardNums.Add(card.Value);
            }

            // Feed the card numbers into a function, along with 4, representing 4 of a kind.
            // If a common number returns as something non-zero, this is a four of a kind. Award 800 points.
            // Add to the points, the value of the common number in order to break ties.
            int commonNum = CheckIfSameNums(listOfCardNums, 4);

            // Find what high card should be used for the fifth card in the poker hand.
            // Remove the common number from the list of numbers.
            foreach(int num in listOfCardNums.ToList())
            {
                if (num == commonNum)
                {
                    listOfCardNums.Remove(num);
                }
            }
            // Use this reduced list to find the highest non-common card.
            int highestCard = 0;
            foreach (int num1 in listOfCardNums)
            {
                int counter = listOfCardNums.Count;

                foreach (int num2 in listOfCardNums)
                {
                    if (num1 >= num2)
                    {
                        counter--;
                    }
                }
                // If a number is larger than all others in the card list, that is the high card.
                if (counter == 0)
                {
                    highestCard = num1;
                    break;
                }
            }
            // If the hand exists, determine the final score by adding 8000, the common number * 20, and the high card value.
            if (commonNum != 0)
            {
                _finalHandScore = 8000 + (commonNum * 20) + highestCard;
                // Set the hand type string for the record.
                _handType = "four of a kind";
            }

        }

        /// <summary>
        /// Checks if a hand is a straight flush. If so, determines score.
        /// </summary>
        private void StraightFlush(List<Card> cardsAvailable)
        {
            // Check if at least 5 of the cards are the same suit.
            string commonSuit = CheckIfCommonSuit(cardsAvailable);

            // If at least 5 of the cards are the same suit, use that suit to do the following check for values.
            if (commonSuit != "noncommon")
            {
                // Check if there are 5 consecutive numbers in the specified suit.
                // Put all the common suit numbers in a list of ints to check for consecutive ones.
                List<int> numbersOfCommonSuit = new List<int>();
                foreach (Card card in cardsAvailable)
                {
                    if ((card.Suit == Suit.Clubs && commonSuit == "clubs") || (card.Suit == Suit.Diamonds && commonSuit == "diamonds") || (card.Suit == Suit.Spades && commonSuit == "spades") || (card.Suit == Suit.Hearts && commonSuit == "hearts"))
                    {
                        numbersOfCommonSuit.Add(card.Value);
                    }
                }

                // consecNums will be true if 5 consecutive numbers are found.
                int consecNums = CheckIfConsecNums(numbersOfCommonSuit);
                // If it is in fact true, and a straight flush has been found, award 9000 points.
                // The highest card in the straight will have its value added to the score for draws.
                if (consecNums != 0)
                {
                    _finalHandScore = 9000 + consecNums;
                    // Set the hand type string for the record.
                    _handType = "straight flush";
                }
            }
        }

        /// <summary>
        /// Checks if a hand is a royal flush. If so, determines score.
        /// </summary>
        private void RoyalFlush(List<Card> cardsAvailable)
        {
            // Check if at least 5 of the cards are the same suit.
            string commonSuit = CheckIfCommonSuit(cardsAvailable);

            // If at least 5 of the cards are the same suit, use that suit to do the following check for values.
            if (commonSuit != "noncommon")
            {
                // Check if there is a 10, Jack(11), Queen(12), King(13), and Ace(14), of the specified suit.
                int counter10 = 0;
                int counterJack = 0;
                int counterQueen = 0;
                int counterKing = 0;
                int counterAce = 0;

                foreach (Card card in cardsAvailable)
                {
                    // If the card is in the common suit, check if it is a 10, J, Q, K, or A. If so, make a count of it.
                    if ((card.Suit == Suit.Clubs && commonSuit == "clubs") || (card.Suit == Suit.Diamonds && commonSuit == "diamonds") || (card.Suit == Suit.Spades && commonSuit == "spades") || (card.Suit == Suit.Hearts && commonSuit == "hearts"))
                    {
                        if (card.Value == 10)
                        {
                            counter10++;
                        }
                        else if (card.Value == 11)
                        {
                            counterJack++;
                        }
                        else if (card.Value == 12)
                        {
                            counterQueen++;
                        }
                        else if (card.Value == 13)
                        {
                            counterKing++;
                        }
                        else if (card.Value == 14)
                        {
                            counterAce++;
                        }
                    }
                }
                // In the event that a royal flush has been found, award 10000 points as the determined final score.
                if (counter10 > 0 && counterJack > 0 && counterQueen > 0 && counterKing > 0 && counterAce > 0)
                {
                    // A royal flush has been found.
                    _finalHandScore = 10000;
                    // Set the hand type string for the record.
                    _handType = "royal flush";
                }
            }
        }

        /// <summary>
        /// Checks if there are five cards with a common suit in a set of 7 available cards.
        /// </summary>
        /// <param name="cardsAvailable"> The cards available to the player's hand. </param>
        /// <returns> A string representing the common suit in this hand, if there is one. Otherwise returns
        /// 'noncommon'. </returns>
        private string CheckIfCommonSuit(List<Card> cardsAvailable)
        {
            int clubCounter = 0;
            int spadeCounter = 0;
            int heartCounter = 0;
            int diamondCounter = 0;

            foreach (Card card in cardsAvailable)
            {
                // Checks the suits of each card available and counts how many of a suit there is.
                switch (card.Suit)
                {
                    case Suit.Clubs:
                        clubCounter++;
                        break;

                    case Suit.Diamonds:
                        diamondCounter++;
                        break;

                    case Suit.Hearts:
                        heartCounter++;
                        break;

                    case Suit.Spades:
                        spadeCounter++;
                        break;

                    default:
                        Debug.Assert(false, "Unknown suit.");
                        break;
                }
            }

            // If there is more than 4 of a single suit, there is a common suit. Return an appropriate string
            // communicating what suit is common, or if no suit is common, return 'noncommon'.
            if (clubCounter > 4)
            {
                return "clubs";
            }
            else if (diamondCounter > 4)
            {
                return "diamonds";
            }
            else if (heartCounter > 4)
            {
                return "hearts";
            }
            else if (spadeCounter > 4)
            {
                return "spades";
            }
            else
            {
                return "noncommon";
            }
        }

        /// <summary>
        /// Checks if there are five consecutive numbers in a hand.
        /// </summary>
        /// <param name="numbersList"> The list of card numbers to be checked. </param>
        /// <returns> Returns the highest number of the 5 cards, if there are 5 consecutive numbers in the number list given.
        ///  Returns 0, if there are no 5 consecutive numbers in the hand. </returns>
        private int CheckIfConsecNums(List<int> numbersList)
        {
            int consecNums = 0;
            // Checks each number in the list for four numbers sequentially higher than it.
            foreach (int cardNum in numbersList)
            {
                // If the number list contains the four numbers higher than the selected number, return consecNums as the highest of the 5.
                if (numbersList.Contains(cardNum + 1) && numbersList.Contains(cardNum + 2) && numbersList.Contains(cardNum + 3) && numbersList.Contains(cardNum + 4))
                {
                    consecNums = (cardNum + 4);
                    break;
                }
            }
            // If no 5 numbers are consecutive in the list, this will return 0.
            return consecNums;
        }

        /// <summary>
        /// Checks if there is a certain number of the same number in a list of numbers.
        /// </summary>
        /// <param name="requiredInCommon"> The number of times the number must occur in the list to be common. </param>
        /// <param name="numbersList"> The list of card numbers to be checked. </param>
        /// <returns> Returns which number was found to be common. Returns 0 if no number was found to be common. </returns>
        private int CheckIfSameNums(List<int> numbersList, int requiredInCommon)
        {
            // For each number in the list, count how many of that number there is in 'counter' int.
            foreach (int num1 in numbersList)
            {
                int counter = 0;

                foreach (int num2 in numbersList)
                {
                    if (num1 == num2)
                    {
                        counter++;
                    }
                }

                // If the required number of common numbers is found, return the specific common number.
                if (counter == requiredInCommon)
                {
                    return num1;
                }
            }

            // No common number was found, return 0.
            return 0;
        }
    }
}
