﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROTPS3
{
    class AIManager
    {
        /// <summary>
        /// list of all the AIProfiles's
        /// </summary>
        private List<AIProfiles> _allProfiles;
        /// <summary>
        /// list of AI's that are accually playing
        /// </summary>
        private List<AI> _aI;
        /// <summary>
        /// list of all AIProfiles that that are made by the user during the settings page
        /// </summary>
        private List<AIProfiles> _profilesAtTable;
        /// <summary>
        /// game object for manipulating methods over there
        /// </summary>
        private Game _game;
        /// <summary>
        /// AIProfileSerializerXML object for accessing the Save and Load later on
        /// </summary>
        private AIProfileSerializerXML _aISerializer;
        /// <summary>
        /// Random object for passing to the AI class so they can use it and not have thesame random.Next() value
        /// </summary>
        private Random _randomizer;
        /// <summary>
        /// constructor for AImanager that will load the user selected AIProfiles if available or set default AI's
        /// it also innitializes it's private variable
        /// </summary>
        /// <param name="game"></param>
        public AIManager(Game game)
        {
            //initializing the appropriate and making the calls that need to happen immediately befor cade runs
            this._game = game;
            _randomizer = new Random();
            _allProfiles = new List<AIProfiles>();
            _aI = new List<AI>();
            _profilesAtTable = new List<AIProfiles>();
            _aISerializer = new AIProfileSerializerXML();

            LoadAIProfiles();
         

        }
        /// <summary>
        /// get and set the feild variable that is of type Random
        /// </summary>
        public Random Randomizer
        {
            get { return _randomizer; }
            set { _randomizer = value; }
        }

        /// <summary>
        /// get and set the feild variable that is of type List<AIProfiles>
        /// </summary>
        public List<AIProfiles> AllProfiles
        {
            get { return _allProfiles; }
            set { _allProfiles=value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type List<AI>
        /// </summary>
        public List<AI> AI
        {
            get { return _aI; }
            set { _aI = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type AIProfileSerializerXML
        /// </summary>
        public AIProfileSerializerXML AISerializer
        {
            get { return _aISerializer; }
            set { _aISerializer = value; }
        }
        /// <summary>
        /// creates the default AI's and adds them to the list of AI's that are actually playing
        /// </summary>
        public void CreatDefaults()
        {
            //setting the AI default properties
            AI artificial = new AI();
            artificial.Name = "iggyboy44";
            artificial.RiskTaker = 0;
            artificial.BigBettor = 0;
            artificial.ScaredyCat = 0;
            artificial.PlayingItSafe = 0;
            artificial.AtTable = 4;
            _aI.Add(artificial);

            AI artificial2 = new AI();
            artificial2.Name = "iggyboy55";
            artificial2.RiskTaker = 0;
            artificial2.BigBettor = 0;
            artificial2.ScaredyCat = 0;
            artificial2.PlayingItSafe = 0;
            artificial2.AtTable = 2;
            _aI.Add(artificial2);

            AI artificial3 = new AI();  // i can't define thesame variable within thesame scope
            artificial3.Name = "iggyboy66";
            artificial3.RiskTaker = 0;
            artificial3.BigBettor = 0;
            artificial3.ScaredyCat = 0;
            artificial3.PlayingItSafe = 0;
            artificial3.AtTable = 3;
            _aI.Add(artificial3);
        }
        /// <summary>
        /// loops through the table AI players and passes them another method for use
        /// </summary>
        public void AIActions()
        {
            
            for (int iplayer=1;iplayer<4;iplayer++)
            {
          
                PassParam(_game.PlayerList[iplayer], _game.Table);
               
            }
        }



        /// <summary>
        /// makes player make a decision and tells the game class what the decision was
        /// </summary>
        /// <param name="player"></param>
        /// <param name="tablecards"></param>
        public void PassParam(Player player, Table tablecards)
        {
            foreach (AI iaI in AI)
            {
                if (iaI.AtTable == player.PlayerNum)
                {
                    //calling method in AI class so that it can make the decid=sion and we can next, find out what it was
                    iaI.MakeDecision(player, tablecards,Randomizer);
                    //these are the possible decision that the AI can make,
                    if (iaI.Decision == "Call")
                    {
                        _game.PlayerAction(1, iaI.AtTable);
                    }
                    //added this so the AI's can't fold during the first round
                    else if (iaI.Decision == "Fold" && _game.DealNum==1)
                    {
                        _game.PlayerAction(1, iaI.AtTable);
                    }
                    else if (iaI.Decision == "Fold")
                    {
                        _game.PlayerAction(3, iaI.AtTable);
                    }
                    else if (iaI.Decision == "Raise")
                    {

                        _game.PlayerAction(2, iaI.AtTable, iaI.AmountToBet);

                    }
                    //added this so thatat the end of each round the AI is able to raise again
                    if (_game.DealNum==4)
                    {
                        iaI.HasRaised = false;
                    }
                }

                    
            } // i think this isn't working rn cus the players havn't actually been made yet. 
        }



        /// <summary>
        /// get the saved AIProfiles on the file and put them all in the list of all the AIProfiles 
        /// </summary>
        public void LoadAIProfiles()
        {
            //if the file is exit then out all the infor of the file in the list _allProfiles and return true
            
            //if the file does not exist then return false
            AISerializer.Load();
            if (AISerializer.AIProfileListing.Count!=0) ///////////////check if there can be jus 2 ai cus that would be incorrect/////////////////////
            {
                
                AllProfiles = AISerializer.AIProfileListing;
                AssignTable();
                ChangeAINames();

            }
            else
            {
                CreatDefaults();
            }
        }

        /// <summary>
        /// Make the AI objects with the AIProfiles properties of the AIProfiles that are actually playing and put all 3 of them in a list 
        /// </summary>
        public void AssignTable()
        {
            //might need a counter for all people at table cus there maight be 2 only witch is wronge
            AI.Clear();
            foreach (AIProfiles iprofile in AllProfiles)  // how does he normallly name theres
            {
                if (iprofile.AtTable > 0)
                {
                    AI artificial = new AI();
                    artificial.Name = iprofile.Name;
                    artificial.RiskTaker = iprofile.RiskTaker;
                    artificial.BigBettor = iprofile.BigBettor;
                    artificial.ScaredyCat = iprofile.ScaredyCat;
                    artificial.PlayingItSafe = iprofile.PlayingItSafe;
                    artificial.AtTable = iprofile.AtTable;
                    _aI.Add(artificial); ////////////////i don't need to set a element limit to this list cus i will make it so that only 3 ai can have atTable = a value over 0///////////////
                }
            }
        }
        /// <summary>
        /// change the player name in the game class to the ones that the user choose
        /// </summary>
        public void ChangeAINames()
        {
            foreach (Player iplayer in _game.PlayerList)
            {
                foreach (AI iAI in AI)  ////////////i think AI should be _ai instead//////////////////////
                {
                    if (iplayer.PlayerNum == iAI.AtTable)
                    {
                        iplayer.Name = iAI.Name;
                    }
                }
            }
        }
    }
}
