﻿// Class: ScorePage.xaml.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Code for the highscore page shown after gameplay.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace ROTPS3
{
    /// <summary>
    /// The highscore page that displays the 10 highest achieved scores after a game.
    /// </summary>
    public sealed partial class ScorePage : Page
    {
        /// <summary>
        /// The game's logic layer, passed in from the game page.
        /// </summary>
        private Game _game;

        /// <summary>
        /// The contructor that initializes the highscore page.
        /// </summary>
        public ScorePage()
        {
            this.InitializeComponent();

            // Initialize the game as null, since it will be passed in by the game page.
            _game = null;

        }

        /// <summary>
        /// Sets the text of all highscores, highlighting the user's score if they recently 
        /// made a new highscore.
        /// </summary>
        private void SetScoreBoxes()
        {

            List<Score> importedHighScoresList = new List<Score>();
            foreach (Score score in _game.HighscoreList)
            {
                importedHighScoresList.Add(score);
            }

            // Order the highscore list from best to worst scores.
            List<Score> orderedHighScoresList = new List<Score>();
            while (importedHighScoresList.Count != 0)
            {
                // This will check if a score is higher than all of the rest, remove it if so,
                // and add it to the ordered list.
                foreach (Score score1 in importedHighScoresList)
                {
                    int counter = importedHighScoresList.Count;
                    foreach (Score score2 in importedHighScoresList)
                    {
                        if (score1.ScoreValue >= score2.ScoreValue)
                        {
                            counter--;
                        }
                    }
                    if (counter == 0)
                    {
                        orderedHighScoresList.Add(score1);
                        importedHighScoresList.Remove(score1);
                        break;
                    }
                }
            }

            // Using highscore list indexes, set the score strings to their appropriate boxes.
            _txtFirstScore.Text = orderedHighScoresList[0].ScoreString;
            _txtSecondScore.Text = orderedHighScoresList[1].ScoreString;
            _txtThirdScore.Text = orderedHighScoresList[2].ScoreString;
            _txtFourthScore.Text = orderedHighScoresList[3].ScoreString;
            _txtFifthScore.Text = orderedHighScoresList[4].ScoreString;
            _txtSixthScore.Text = orderedHighScoresList[5].ScoreString;
            _txtSeventhScore.Text = orderedHighScoresList[6].ScoreString;
            _txtEighthScore.Text = orderedHighScoresList[7].ScoreString;
            _txtNinthScore.Text = orderedHighScoresList[8].ScoreString;
            _txtTenthScore.Text = orderedHighScoresList[9].ScoreString;

            // If a recent user score is included, highlight its appropriate box.
            for(int userScoreIndex = 0; userScoreIndex < 10; userScoreIndex++)
            {
                if (orderedHighScoresList[userScoreIndex].RecentUserScore == true)
                {
                    switch(userScoreIndex)
                    {
                        case 0:
                            _txtFirstScore.Style = Resources["TextBlockHighlightedStyle"] as Style;
                            _border1.Style = Resources["BorderHighlightedStyle"] as Style;
                            break;

                        case 1:
                            _txtSecondScore.Style = Resources["TextBlockHighlightedStyle"] as Style;
                            _border2.Style = Resources["BorderHighlightedStyle"] as Style;
                            break;

                        case 2:
                            _txtThirdScore.Style = Resources["TextBlockHighlightedStyle"] as Style;
                            _border3.Style = Resources["BorderHighlightedStyle"] as Style;
                            break;

                        case 3:
                            _txtFourthScore.Style = Resources["TextBlockHighlightedStyle"] as Style;
                            _border4.Style = Resources["BorderHighlightedStyle"] as Style;
                            break;

                        case 4:
                            _txtFifthScore.Style = Resources["TextBlockHighlightedStyle"] as Style;
                            _border5.Style = Resources["BorderHighlightedStyle"] as Style;
                            break;

                        case 5:
                            _txtSixthScore.Style = Resources["TextBlockHighlightedStyle"] as Style;
                            _border6.Style = Resources["BorderHighlightedStyle"] as Style;
                            break;

                        case 6:
                            _txtSeventhScore.Style = Resources["TextBlockHighlightedStyle"] as Style;
                            _border7.Style = Resources["BorderHighlightedStyle"] as Style;
                            break;

                        case 7:
                            _txtEighthScore.Style = Resources["TextBlockHighlightedStyle"] as Style;
                            _border8.Style = Resources["BorderHighlightedStyle"] as Style;
                            break;

                        case 8:
                            _txtNinthScore.Style = Resources["TextBlockHighlightedStyle"] as Style;
                            _border9.Style = Resources["BorderHighlightedStyle"] as Style;
                            break;

                        case 9:
                            _txtTenthScore.Style = Resources["TextBlockHighlightedStyle"] as Style;
                            _border10.Style = Resources["BorderHighlightedStyle"] as Style;
                            break;

                        default:
                            Debug.Assert(false, "Error, highscore out of range for page.");
                            break;

                    }
                    break;
                }
            }
        }

        /// <summary>
        /// When the play again button is pressed, this will be called to restart the game.
        /// </summary>
        /// <param name="sender"> The play again button. </param>
        /// <param name="e"> Arguments related to the play again button. </param>
        private void OnPlayAgain(object sender, RoutedEventArgs e)
        {
            // Reset values from the last game.
            _game.Reset();

            Frame.Navigate(typeof(MainPage), _game);
        }

        /// <summary>
        /// When navigated to from the game page, the game object will be passed in.
        /// Then, the highscore textblocks will be appropriately changed.
        /// </summary>
        /// <param name="e"> The game object from the game page. </param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Take in the game object passed in by the game page.
            _game = e.Parameter as Game;

            // Set highscore textblocks.
            SetScoreBoxes();
        }
    }
}
