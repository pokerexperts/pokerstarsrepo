﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ROTPS3
{
    [DataContract]
    public class AIProfiles
    {
        [DataMember]
        private string _name;
        [DataMember]
        private int _riskTaker;
        [DataMember]
        private int _bigBettor;
        [DataMember]
        private int _scaredyCat;
        [DataMember]
        private int _playingItSafe;
        [DataMember]
        private int _atTable;
        [DataMember]
        private int _id;
        [DataMember]
        private Dictionary<string, Dictionary<string, double>> _aIContainer;
        public AIProfiles()
        {
  
        }
        public Dictionary<string, Dictionary<string, double>> AIContainer
        {
            get { return _aIContainer; }
            set { _aIContainer = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public int RiskTaker
        {
            get { return _riskTaker; }
            set { _riskTaker = value; }
        }
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int BigBettor
        {
            get { return _bigBettor; }
            set { _bigBettor = value; }
        }

        public int ScaredyCat
        {
            get { return _scaredyCat; }
            set { _scaredyCat = value; }
        }

        public int PlayingItSafe
        {
            get { return _playingItSafe; }
            set { _playingItSafe = value; }
        }



        public int AtTable
        {
            get { return _atTable; }
            set { _atTable = value; }
        }

        public void Save(string filename)
        {

            
            using (FileStream AIProfilesStream = new FileStream(filename, FileMode.Create))
            {
                DataContractSerializer reader = new DataContractSerializer(typeof(AIProfiles));
                XmlDictionaryWriter xdw = XmlDictionaryWriter.CreateTextWriter(AIProfilesStream, Encoding.UTF8);
                reader.WriteObject(AIProfilesStream, this);
            }
        }
        public static AIProfiles Load(string filename)
        {
            using (FileStream AIProfilesStream = new FileStream(filename, FileMode.Open))
            {
                using (XmlDictionaryReader reader= XmlDictionaryReader.CreateTextReader(AIProfilesStream,new XmlDictionaryReaderQuotas()))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(AIProfiles));
                    AIProfiles profile = serializer.ReadObject(reader, true) as AIProfiles;
                    return profile;
                }
                
            }
        }

     
    }
}
