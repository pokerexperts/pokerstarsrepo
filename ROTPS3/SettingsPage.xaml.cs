﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace ROTPS3
{
    /// <summary>
    /// A list of AIProfiles that extend with each new AI that the user chooses to make
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        /// <summary>
        /// The manager of the game and their settings.
        /// </summary>
        private Game _game;
        /// <summary>
        /// list of all the AIProfiles's
        /// </summary>
        private List<AIProfiles> _aIProfileList;
        /// <summary>
        /// this varible's key is the ids of the AIProfiles objects
        /// and it uses the ids for getting the keys in the MainAIContainer
        /// </summary>
        private Dictionary<string, string> _allIds;
        /// <summary>
        /// for storing the current AIProfiles choosen traits that is to be stored later in the MainAIContainer,all except the name 
        /// </summary>
        private Dictionary<string, double> _traitHolder;

        /// <summary>
        /// for storing all the choosen traits that the user has chossen for each AIProfile object in total
        /// </summary>
        private Dictionary<string, Dictionary<string, double>> _mainAIContainer;  // name these dictionary better
        /// <summary>
        /// for storing the name that the user inputs 
        /// </summary>
        private string[] _currentNameHolder;
        /// <summary>
        /// for storing the temporary removed dictionary from the _mainAIContainer variable 
        /// </summary>
        private Dictionary<string, double> _traitHolderComponent;
        /// <summary>
        /// storing the current number that can increase by 1 over time
        /// </summary>
        private int _mainNameCounter=1;                     ///////////////////////////////we did not use this at all/////////////////
        /// <summary>
        /// storing the current number that can increase by 1 over time
        /// </summary>
        private int _mainCounter = 1;
        /// <summary>
        /// AIProfileSerializerXML object for accessing the Save and Load later on
        /// </summary>
        private AIProfileSerializerXML _aISerializer;
        /// <summary>
        ///  constructor for the SettingsPage that initializers it's feilds, also
        ///  makes default listview items through loading from a file and then doing so or if 
        ///  thats not possible then making default through calling a mr=ethod that it has
        /// </summary>
        public SettingsPage()
        {
            this.InitializeComponent();
            // this is null because it is set in the navigateion
            _game = null;
            _allIds = new Dictionary<string, string>();
            _aIProfileList = new List<AIProfiles>();
           
            _traitHolder = new Dictionary<string, double>();
            _traitHolderComponent = new Dictionary<string, double>();
            _mainAIContainer = new Dictionary<string, Dictionary<string, double>>();
            _currentNameHolder = new string[1];
            
            _aISerializer = new AIProfileSerializerXML();
            //make default Ai's
            MakeDefaultAI();        //////////why would we make default Ai's here?
            DisplayDefaultAI();
            //load Ai's from file
            Load();
            //
            LinkNewALLIds();
            DisplayDefaultAI();

        }


        /// <summary>
        /// get and set the feild variable that is of type AIProfileSerializerXML
        /// </summary>
        private AIProfileSerializerXML AISerializer
        {
            get {return _aISerializer; }
            set { _aISerializer=value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type Dictionary<string, Dictionary<string, double>>
        /// </summary>
        public Dictionary<string, Dictionary<string, double>> MainAIContainer
        {
            get { return _mainAIContainer; }
            set { _mainAIContainer = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type int
        /// </summary>
        public int MainNameCounter                      ////////////////////////we did not use this at all//////////////////
        {
            get { return _mainNameCounter; }
            set { _mainNameCounter = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type int
        /// </summary>
        public int MainCounter
        {
            get { return _mainCounter; }
            set { _mainCounter = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type List<AIProfiles>
        /// </summary>
        public List<AIProfiles> AIProfileList
        {
            get {return _aIProfileList; }
            set { _aIProfileList = value; }
        }
        /// <summary>
        ///  get and set the feild variable that is of type Dictionary<string, double>
        /// </summary>
        public Dictionary<string, double> TraitHolder
        {
            get { return _traitHolder; }
            set { _traitHolder = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type Dictionary<string, double>
        /// </summary>
        public Dictionary<string, double> TraitHolderComponent
        {
            get { return _traitHolderComponent; }
            set { _traitHolderComponent = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type Dictionary<string, string>
        /// </summary>
        public Dictionary<string, string> AllIds
        {
            get { return _allIds; }
            set { _allIds = value; }
        }

        /// <summary>
        /// get and set the feild variable that is of type string[]
        /// </summary>
        private string[] CurrentNameHolder
        {
            get { return _currentNameHolder; }
            set { _currentNameHolder = value; }
        }
        /// <summary>
        /// adding AIProfiles to the dictionary
        /// </summary>
        public void LinkNewALLIds()
        {
            AllIds.Clear();
            foreach (AIProfiles iprofile in AIProfileList)
            {
                AllIds.Add(iprofile.Id.ToString(), iprofile.Name.ToString());
            }
        }

        /// <summary>
        /// displays button if there is enough AIProfiles in the _aIProfileList
        /// </summary>
        private void CheckToEnable()
        {
            int checkingif = TraitHolder.Count;
            //check the number of choosen traits and if if 4 or mor ethen make the radio button clickable
            if (TraitHolder.Count == 4 || TraitHolder.Count == 5)
            {
                _btnMakeChanges.IsEnabled = true;
                _radbtnPerson2.IsEnabled = true;
                _radbtnPerson3.IsEnabled = true;
                _radbtnPerson4.IsEnabled = true;
            }
        }
        /// <summary>
        /// store the choosen traits for the selected AIProfiles on the MainAIContainer to have a place where all the information is stored
        /// </summary>
        private void SetNewDict()   ///////////////////resposible for less at table players////////////////////////
        {
            ////////////////////////////4 traist and i changed my old name///////////////////////////
            //right now theses r doesn't equal it or does so i need to cover thecase that it does
            if (TraitHolder.Count == 4 && CurrentNameHolder[0] != null) //the default empty value for a stringed dynamic array is null so ofcourse it will have a leength of 1 still
            {
                /////////////check if im already existing,if so get the "At Table" value that i had prior and set it to this 1 as well

                //ChoiceAnalysis("At Table", 0);
                //set the tale possition to zero
                //ChoiceAnalysis("At Table", 0);       //////redundant code:this code should not exist cus it sets it back to 0////////////////////
                //make changes
                foreach (AIProfiles icheckprofile in AIProfileList)
                {
                    //check the Id of the AIProfiles 
                    if (icheckprofile.Id == ((AIProfiles)_lstAIPersons.SelectedItem).Id)
                    {
                        ChoiceAnalysis("At Table", icheckprofile.AtTable);     ////////////////fix this like i stated above///////////////////////////
                    }
                }

                AIProfiles jjj = (AIProfiles)_lstAIPersons.SelectedItem;
                //and its n0t as the one in the id list, remove the reference in Trait holder and remove the name in id and change to this new one and change traitholder to his new one
                MainAIContainer.Remove(AllIds[((AIProfiles)_lstAIPersons.SelectedItem).Id.ToString()]);

                //add selected user name as the key of the MainAIContainer
                MainAIContainer.Add(CurrentNameHolder[0], new Dictionary<string, double>());
                foreach (string ikey in TraitHolder.Keys)
                {
                    //add selected user name as the key of the MainAIContainer
                    MainAIContainer[CurrentNameHolder[0]].Add(ikey, TraitHolder[ikey]);
                }
                //make if statement for if their is already a key value with that name
                AllIds[((AIProfiles)_lstAIPersons.SelectedItem).Id.ToString()] = CurrentNameHolder[0];
                ///////////////////use this for loop on the next block of code too cus u hae to when u make changes/////////////////
                ////////////////////////we don't need this for our first case////////////////
                //////////////i shouldn't need the below code for this yet but wel leave it////////////////
                //foreach (AIProfiles icheckprofile in AIProfileList)
                //{
                    //check the Id of the AIProfiles 
                   //////////////////////check if they share thesame at table value and switch em/////////////////////
                    //if (icheckprofile.AtTable !=0 && icheckprofile.AtTable == (int)TraitHolder["At Table"])
                    //{
                        //icheckprofile.AtTable = ((AIProfiles)_lstAIPersons.SelectedItem).AtTable;
                    //}
                //}
                //////////////////change this selected items inside value////////////////////////////////
                foreach (AIProfiles icheckprofile in AIProfileList)
                {
                    //check the Id of the AIProfiles 
                    if (icheckprofile.Id == ((AIProfiles)_lstAIPersons.SelectedItem).Id)
                    {
                        icheckprofile.Name = CurrentNameHolder[0];
                        icheckprofile.AtTable = (int)TraitHolder["At Table"]; ///////////could be very wronge type casting cus its for a double///////
                        icheckprofile.RiskTaker = (int)TraitHolder["Risk Taker"];
                        icheckprofile.BigBettor = (int)TraitHolder["Big Bettor"];
                        icheckprofile.ScaredyCat = (int)TraitHolder["ScaredyCat"];
                        icheckprofile.PlayingItSafe = (int)TraitHolder["Playing It Safe"];
                        ///////make icheckprofile.AIContainer = MainAIContainer;/////////
                        /////////is there a point to doing this,my comment below i mean?///////////////
                        //////////////////i should change everything else withing AIProfiles this too write?,check how i saved for answer///////////////////////
                    }

                }







                ///////////////////use this for loop on the next block of code too cus u hae to when u make changes/////////////////
                //clear items in the listview
                //_lstAIPersons.Items.Clear();
                //make the AIProfiles show up in listview
                DisplayDefaultAI();


            }   //////////////////////////////i just change 4 traits///////////////////////
            else if (TraitHolder.Count == 4 && (CurrentNameHolder[0] == null))
            {

                /////////use thesame ordering as the above similar code/////////////////////////////////////
                /////////////check if im already existing,if so get the "At Table" value that i had prior and set it to this 1 as well
                MainAIContainer.Remove(AllIds[((AIProfiles)_lstAIPersons.SelectedItem).Id.ToString()]);
                // get name from AIProfile
                string assignedName = ((AIProfiles)_lstAIPersons.SelectedItem).Name;

                //set the tale possition to zero cus the user didn't set a "AT Table" trait during the ui selection 
                foreach (AIProfiles icheckprofile in AIProfileList)
                {
                    //check the Id of the AIProfiles 
                    if (icheckprofile.Id == ((AIProfiles)_lstAIPersons.SelectedItem).Id)
                    {
                        ChoiceAnalysis("At Table", icheckprofile.AtTable);     ////////////////fix this like i stated above///////////////////////////
                    }
                }
                        //make changes
                MainAIContainer.Add(assignedName, new Dictionary<string, double>());
                //find the TraitHolder dictionary value to the MainAIContainer dictionary
                foreach (string ikey in TraitHolder.Keys)
                {
                    MainAIContainer[assignedName].Add(ikey, TraitHolder[ikey]);///////////////////////fixed now i think:add _lstAIPersons.SelectedItem.AtTable to the last key
              
                }
                AllIds[((AIProfiles)_lstAIPersons.SelectedItem).Id.ToString()] = assignedName;

                //////////////////change this selected items inside value////////////////////////////////
                foreach (AIProfiles icheckprofile in AIProfileList)
                {
                    //check the Id of the AIProfiles 
                    if (icheckprofile.Id == ((AIProfiles)_lstAIPersons.SelectedItem).Id)
                    {
                        //icheckprofile.Name = CurrentNameHolder[0];
                        icheckprofile.AtTable = (int)TraitHolder["At Table"]; ///////////could be very wronge type casting cus its for a double///////
                        icheckprofile.RiskTaker = (int)TraitHolder["Risk Taker"];
                        icheckprofile.BigBettor = (int)TraitHolder["Big Bettor"];
                        icheckprofile.ScaredyCat = (int)TraitHolder["ScaredyCat"];
                        icheckprofile.PlayingItSafe = (int)TraitHolder["Playing It Safe"];
                        ///////make icheckprofile.AIContainer = MainAIContainer;/////////
                        /////////is there a point to doing this,my comment below i mean?///////////////
                        //////////////////i should change everything else withing AIProfiles this too write?,check how i saved for answer///////////////////////
                    }

                }
                //clear items in the listview
                //_lstAIPersons.Items.Clear();
                //make the AIProfiles show up in listview
                DisplayDefaultAI();

                //////////////////i should change everything else withing AIProfiles this too write?,check how i saved for answer///////////////////////
            }
            ///////////////////i just change 5 traits only and didn't select a name even tho i had 1 befor this////////////////////
            ///check if the user has seleccted a name as well as 4 other traits if so run this 
            else if (TraitHolder.Count == 5 && CurrentNameHolder[0] != null)
            {
                

                foreach (string ikey in MainAIContainer.Keys)
                {

                    int g = (int)TraitHolder["At Table"];
                    int k = (int)MainAIContainer[ikey]["At Table"];
                    //change the value of the "At Table" key in MainAIContainer if it has the same "AT Table value as the Traitholder that the user just filled up
                    if (MainAIContainer[ikey]["At Table"] == TraitHolder["At Table"])
                    {
                        MainAIContainer[ikey]["At Table"] = (double)(((AIProfiles)_lstAIPersons.SelectedItem).AtTable); /////////////////this line has risk of errorrrrrrrrrrr/////////////////

                        ///////////////////i need to also exchange my pass at table value with them//////////////////
                    }

                }


                //remove remove the entry from the current selected person
                MainAIContainer.Remove(AllIds[((AIProfiles)_lstAIPersons.SelectedItem).Id.ToString()]);
                //add a new key and dictionary value to the MainContainer cus this is what will be used in the saving of the AIProfiles laer when 
                //the player leaves the Settings page
                MainAIContainer.Add(CurrentNameHolder[0], new Dictionary<string, double>());
                foreach (string ikey in TraitHolder.Keys)
                {
                    MainAIContainer[CurrentNameHolder[0]].Add(ikey, TraitHolder[ikey]);
                }
                //set the current selected _lstAIPersons key value in the dictionary to be equal to the current name that the user has choosen
                //this is so the ALLids dictionary is updated on what the new dictionary key value in MainAIContainer is
                AllIds[((AIProfiles)_lstAIPersons.SelectedItem).Id.ToString()] = CurrentNameHolder[0];
                //change the names of all the AIProfiles in the list so that when u diplay them later the new names can show up in the listview

                foreach (AIProfiles icheckprofile in AIProfileList)
                {
                    //check the Id of the AIProfiles 
                    //////////////////////check if they share thesame at table value and switch em/////////////////////

                

                    if (icheckprofile.AtTable != 0 && icheckprofile.AtTable == (int)TraitHolder["At Table"])
                    {
                        icheckprofile.AtTable = ((AIProfiles)_lstAIPersons.SelectedItem).AtTable;
                    }
                }

                foreach (AIProfiles icheckprofile in AIProfileList)
                {
                    //check the Id of the AIProfiles 
                    if (icheckprofile.Id == ((AIProfiles)_lstAIPersons.SelectedItem).Id)
                    {
                        icheckprofile.Name = CurrentNameHolder[0];
                        icheckprofile.AtTable = (int)TraitHolder["At Table"]; ///////////could be very wronge type casting cus its for a double///////
                        icheckprofile.RiskTaker = (int)TraitHolder["Risk Taker"];
                        icheckprofile.BigBettor = (int)TraitHolder["Big Bettor"];
                        icheckprofile.ScaredyCat = (int)TraitHolder["ScaredyCat"];
                        icheckprofile.PlayingItSafe = (int)TraitHolder["Playing It Safe"];
                        ///////make icheckprofile.AIContainer = MainAIContainer;/////////
                        /////////is there a point to doing this,my comment below i mean?///////////////
                        //////////////////i should change everything else withing AIProfiles this too write?,check how i saved for answer///////////////////////
                    }

                }
                //clear the items in the listview so you can store new items there
                //_lstAIPersons.Items.Clear();
                DisplayDefaultAI();
            }

            ///////////////////i just change 5 traits only and didn't select a name and the past name was also null////////////////////
            //////////this might be wronge///////////
            else if (TraitHolder.Count == 5 && (CurrentNameHolder[0] == null || AllIds[((AIProfiles)_lstAIPersons.SelectedItem).Id.ToString()] == CurrentNameHolder[0]))
            {

                foreach (string ikey in MainAIContainer.Keys)
                {
                    int g = (int)TraitHolder["At Table"];
                    int k = (int)MainAIContainer[ikey]["At Table"];
                    //change the value of the "At Table" key in MainAIContainer if it has the same "AT Table value as the Traitholder that the user just filled up
                    if (MainAIContainer[ikey]["At Table"] == TraitHolder["At Table"])
                    {
                        MainAIContainer[ikey]["At Table"] = (double)(((AIProfiles)_lstAIPersons.SelectedItem).AtTable);
                        ///////////////////i need to also exchange my pass at table value with them//////////////////
                    }

                }
                //remove remove the entry from the current selected person
                MainAIContainer.Remove(AllIds[((AIProfiles)_lstAIPersons.SelectedItem).Id.ToString()]);
                string assignedName = ((AIProfiles)_lstAIPersons.SelectedItem).Name;
                //make changes
                //add a new key and dictionary value to the MainContainer cus this is what will be used in the saving of the AIProfiles laer when 
                //the player leaves the Settings page
                MainAIContainer.Add(assignedName, new Dictionary<string, double>());
                foreach (string ikey in TraitHolder.Keys)
                {
                    MainAIContainer[assignedName].Add(ikey, TraitHolder[ikey]);
                }
                AllIds[((AIProfiles)_lstAIPersons.SelectedItem).Id.ToString()] = assignedName;

                foreach (AIProfiles icheckprofile in AIProfileList)
                {
                    //check the Id of the AIProfiles 
                    //////////////////////check if they share thesame at table value and switch em/////////////////////

                   

                    if (icheckprofile.AtTable != 0 && icheckprofile.AtTable == (int)TraitHolder["At Table"])
                    {
                        icheckprofile.AtTable = ((AIProfiles)_lstAIPersons.SelectedItem).AtTable;
                    }
                }

                //////////////////i should change everything else withing AIProfiles this too write?,check how i saved for answer///////////////////////
                foreach (AIProfiles icheckprofile in AIProfileList)
                {
                    //check the Id of the AIProfiles 
                    if (icheckprofile.Id == ((AIProfiles)_lstAIPersons.SelectedItem).Id)
                    {
                        icheckprofile.Name = assignedName;
                        icheckprofile.AtTable = (int)TraitHolder["At Table"]; ///////////could be very wronge type casting cus its for a double///////
                        icheckprofile.RiskTaker = (int)TraitHolder["Risk Taker"];
                        icheckprofile.BigBettor = (int)TraitHolder["Big Bettor"];
                        icheckprofile.ScaredyCat = (int)TraitHolder["ScaredyCat"];
                        icheckprofile.PlayingItSafe = (int)TraitHolder["Playing It Safe"];
                        ///////make icheckprofile.AIContainer = MainAIContainer;/////////
                        /////////is there a point to doing this,my comment below i mean?///////////////
                        //////////////////i should change everything else withing AIProfiles this too write?,check how i saved for answer///////////////////////
                    }

                }
                //clear the items in the listview so you can store new items there
                //_lstAIPersons.Items.Clear();
                DisplayDefaultAI();
            }
            else
            {
                //display message to let user know they must fill out all trait if they are to make any changes
            }
        }
        /// <summary>
        /// delete a _lstAIPersons listview item and updates it so it is not shown anymore 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDeleteAI(object sender, RoutedEventArgs e)
        {                                                               ////////////////////shouldn't i make a list of all the removed ids and use em b4 using the original counter again?//////////////////////////
            if (MainAIContainer.Count>3)
            {
                if (((AIProfiles)_lstAIPersons.SelectedItem).AtTable != 2 && ((AIProfiles)_lstAIPersons.SelectedItem).AtTable != 3 && ((AIProfiles)_lstAIPersons.SelectedItem).AtTable != 4)
                {
                    ///////////////////////check if the selected for delete ai is choosen to be at table/////////////////////

                    //remove remove the entry from the current selected person
                    MainAIContainer.Remove(AllIds[((AIProfiles)_lstAIPersons.SelectedItem).Id.ToString()]);
                    //foreach(AIProfiles iprofile in AIProfileList)
                    //{
                    //check if the id's are thesame and remove profile from the list if so
                    //if (iprofile.Id==((AIProfiles)_lstAIPersons.SelectedItem).Id )
                    //{
                    //AIProfileList.Remove(iprofile);

                    //List<AIProfiles> g = AIProfileList; /////////////////why do we need this line of code/////////////////
                    //break;
                    //}
                    //}
                    for (int indexAI = 0; indexAI < AIProfileList.Count; indexAI++)
                    {
                        if (AIProfileList[indexAI].Id == ((AIProfiles)_lstAIPersons.SelectedItem).Id)
                        {
                            AIProfileList.RemoveAt(indexAI);

                            List<AIProfiles> g = AIProfileList; /////////////////why do we need this line of code/////////////////
                            break;
                        }
                    }
                    //clearing the _lstAIPersons so it can be repopulated with 1 less items
                    //_lstAIPersons.Items.Clear();
                    DisplayDefaultAI();
                }
                else
                {
                    MessageDialog msgDlg = new MessageDialog("Can't delete an Ai player placed at the table");
                    msgDlg.ShowAsync();
                }
                
                
            }
            else
            {
                MessageDialog msgDlg = new MessageDialog("Must have at least 3 Ai players to delete");
                msgDlg.ShowAsync();

            }
        }
        /// <summary>
        /// call a methosod that makes the changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMakeChanges(object sender, RoutedEventArgs e)
        {
            SetNewDict();

        }




        /// <summary>
        /// adds a "At Table" key and it's choosen value to Traitholder here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTablePlayer(object sender, RoutedEventArgs e)
        {
            AIProfiles selectedAccount=(AIProfiles)_lstAIPersons.SelectedItem;
            if (sender==_radbtnPerson2)
            {
                ChoiceAnalysis("At Table",2);
            }
            else if (sender==_radbtnPerson3)
            {
                ChoiceAnalysis("At Table",3);
            }
            else if (sender==_radbtnPerson4)
            {
                ChoiceAnalysis("At Table",4);
            }
            else
            {
                //debug code stuff
            }
            

        }

        /// <summary>
        /// to display the 5 items in the listview beside it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectAI(object sender, SelectionChangedEventArgs e)
        {
            //setting these two to false to make sure they can't be enabled yet,
            _txtInputName.IsEnabled = false;
            _btnMakeChanges.IsEnabled = false;
            //clearing out the TraitHolder incase there is new left over values in there and we don't want that to inturrupt this new selected listView item
            TraitHolder.Clear();
            CurrentNameHolder[0] = null;

            AIProfiles selectedAccount = _lstAIPersons.SelectedItem as AIProfiles;
         
            //_lstAICharacteristics.SelectedItem = false;
            _lstAICharacteristics.Items.Clear();

            _lstAICharacteristics.Items.Add("Risk Taker");
            _lstAICharacteristics.Items.Add("Big Bettor");
            _lstAICharacteristics.Items.Add("ScaredyCat");
            _lstAICharacteristics.Items.Add("Playing It Safe");
            _lstAICharacteristics.Items.Add("Name");

            //_grdAITrait.IsEnabled = false;
            _grdAITrait.IsEnabled = false;

        }
        /// <summary>
        /// enable the purple gridview or the textbox based of the users selected trait
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCharacteristicSelect(object sender, SelectionChangedEventArgs e)
        {
            int checkingif = TraitHolder.Count;

            string choosenTrait = (string)_lstAICharacteristics.SelectedItem;  //Note to self:for list view object cast them dont just .Tostring() it cus that don't work
            if (choosenTrait == "Name")
            {

                _grdAITrait.IsEnabled = false;
                _txtInputName.IsEnabled = true;
            }
            else
            {
                _grdAITrait.IsEnabled = true;
                
            }

        }

        private void OnChosenTrait(object sender, SelectionChangedEventArgs e)
        {
            int checkingif = TraitHolder.Count;

            AIProfiles selectedPerson = (AIProfiles)_lstAIPersons.SelectedItem;
            

            TextBlock selectedButton = _grdAITrait.SelectedItem as TextBlock;
            ///checking what the selected Trait was so i can make the appropriate decision and know what to call next
            if (selectedButton.Name == "_BtnTraitSelected1")
            {
                //set the trait choosen by the user to 5 if they click button that has a +%5 on it
                ChoiceAnalysis(_lstAICharacteristics.SelectedItem.ToString(), 5);
                //call function to check if there is at least 4 trait holders inputed into the Traitholder dictionary
                CheckToEnable();
            }
            else if (selectedButton.Name == "_BtnTraitSelected2")
            {
                //set the trait choosen by the user to 10 if they click button that has a +%10 on it
                ChoiceAnalysis(_lstAICharacteristics.SelectedItem.ToString(), 10);
                //call function to check if there is at least 4 trait holders inputed into the Traitholder dictionary
                CheckToEnable();
            }
            else if (selectedButton.Name == "_BtnTraitSelected3")
            {
                //set the trait choosen by the user to 15 if they click button that has a +%15 on it
                ChoiceAnalysis(_lstAICharacteristics.SelectedItem.ToString(), 15);
                //call function to check if there is at least 4 trait holders inputed into the Traitholder dictionary
                CheckToEnable();
            }
            else
            {
                
            }

        }

        /// <summary>
        /// make the default AIProfiles and put them in the listview and the MainAIContainer dicionary
        /// </summary>
        private void MakeDefaultAI()
        {
            //make AI with all the required properties set to zero
            //add the trait that u just set th=o the Trait holder dictionary so it can later be added to the MainAIContainer
            AIProfiles aIProfile1 = new AIProfiles();
            aIProfile1.Name = "Sam";
            CurrentNameHolder[0] = aIProfile1.Name;
            aIProfile1.RiskTaker = 0;
            TraitHolder.Add("Risk Taker", (double)aIProfile1.RiskTaker);
            aIProfile1.BigBettor = 0;
            TraitHolder.Add("Big Bettor", (double)aIProfile1.BigBettor);
            aIProfile1.ScaredyCat = 0;
            TraitHolder.Add("ScaredyCat", (double)aIProfile1.ScaredyCat);
            aIProfile1.PlayingItSafe = 0;
            TraitHolder.Add("Playing It Safe", (double)aIProfile1.PlayingItSafe);
            aIProfile1.AtTable = 2;
            TraitHolder.Add("At Table", (double)aIProfile1.AtTable);
            MainAIContainer.Add(CurrentNameHolder[0], new Dictionary<string, double>());
            foreach (string ikey in TraitHolder.Keys)
            {
                MainAIContainer[CurrentNameHolder[0]].Add(ikey, TraitHolder[ikey]);
            }
            MainCounterSavedValue();
            aIProfile1.Id = MainCounter;
            AllIds[aIProfile1.Id.ToString()] = CurrentNameHolder[0];
            //aIProfile1.AIContainer = MainAIContainer;
            AIProfileList.Add(aIProfile1);
            MainCounterSavedValue();
            TraitHolder.Clear();
            CurrentNameHolder[0] = null;

            //make AI with all the required properties set to zero
            //add the trait that u just set th=o the Trait holder dictionary so it can later be added to the MainAIContainer
            AIProfiles aIProfile2 = new AIProfiles();
            aIProfile2.Name = "Steve";
            CurrentNameHolder[0] = aIProfile2.Name;
            aIProfile2.RiskTaker = 0;
            TraitHolder.Add("Risk Taker", (double)aIProfile2.RiskTaker);
            aIProfile2.BigBettor = 0;
            TraitHolder.Add("Big Bettor", (double)aIProfile2.BigBettor);
            aIProfile2.ScaredyCat = 0;
            TraitHolder.Add("ScaredyCat", (double)aIProfile2.ScaredyCat);
            aIProfile2.PlayingItSafe = 0;
            TraitHolder.Add("Playing It Safe", (double)aIProfile2.PlayingItSafe);
            aIProfile2.AtTable = 3;
            TraitHolder.Add("At Table", (double)aIProfile2.AtTable);
            MainAIContainer.Add(CurrentNameHolder[0], new Dictionary<string, double>());
            foreach (string ikey in TraitHolder.Keys)
            {
                MainAIContainer[CurrentNameHolder[0]].Add(ikey, TraitHolder[ikey]);
            }
            MainCounterSavedValue();
            aIProfile2.Id = MainCounter;
            AllIds[aIProfile2.Id.ToString()] = CurrentNameHolder[0];
            //aIProfile2.AIContainer = MainAIContainer;
            AIProfileList.Add(aIProfile2);
            MainCounterSavedValue();
            TraitHolder.Clear();
            CurrentNameHolder[0] = null;

            //make AI with all the required properties set to zero
            //add the trait that u just set th=o the Trait holder dictionary so it can later be added to the MainAIContainer
            AIProfiles aIProfile3 = new AIProfiles();
            aIProfile3.Name = "Iggy";
            CurrentNameHolder[0] = aIProfile3.Name;
            aIProfile3.RiskTaker = 0;
            TraitHolder.Add("Risk Taker", (double)aIProfile3.RiskTaker);
            aIProfile3.BigBettor = 0;
            TraitHolder.Add("Big Bettor", (double)aIProfile3.BigBettor);
            aIProfile3.ScaredyCat = 0;
            TraitHolder.Add("ScaredyCat", (double)aIProfile3.ScaredyCat);
            aIProfile3.PlayingItSafe = 0;
            TraitHolder.Add("Playing It Safe", (double)aIProfile3.PlayingItSafe);
            aIProfile3.AtTable = 4;
            TraitHolder.Add("At Table", (double)aIProfile3.AtTable);
            MainAIContainer.Add(CurrentNameHolder[0], new Dictionary<string, double>());
            foreach (string ikey in TraitHolder.Keys)
            {
                MainAIContainer[CurrentNameHolder[0]].Add(ikey, TraitHolder[ikey]);
            }
            MainCounterSavedValue();
            aIProfile3.Id = MainCounter;
            AllIds[aIProfile3.Id.ToString()] = CurrentNameHolder[0];
            //aIProfile3.AIContainer = MainAIContainer;
            AIProfileList.Add(aIProfile3);
            //MainCounter = MainCounter + 1;
            TraitHolder.Clear();
            CurrentNameHolder[0] = null;

        }
        /// <summary>
        /// save the AIProfilesList throught calling another method in a defferent class
        /// </summary>
        public void Save()
        {
            //save the AIProfile's if the AIProfileList has at least 3 elements in it
            if (AIProfileList.Count >= 3)
            {
                
                AISerializer.AIProfileListing = AIProfileList;
                //save the AIProfiles
                AISerializer.Save();
            }

        }
        /// <summary>
        /// load the AIProfiles from the file to the AIProfileList list
        /// </summary>
        public void Load()
        {

            AISerializer.Load();
            if (AISerializer.AIProfileListing.Count != 0)
            {
                AIProfileList = AISerializer.AIProfileListing;
                //int totalAIs = AIProfileList.Count;

                MainAIContainer = AIProfileList[0].AIContainer;

                //MainAIContainer = AIProfileList[totalAIs-1].AIContainer;


                //MainCounter = AIProfileList.Count + 1;    //////////////check what count returns

                /////////////////call a method for the maincounter to have it's past saved game value also //////////////////
                // give maincounter thesame value, if user adding an ai it will 
            }

        }

        private void MainCounterSavedValue()
        {
            int max = 0;
            foreach (AIProfiles icheckprofile in AIProfileList)
            {
                //check the Id of the AIProfiles 
                if (icheckprofile.Id > max)
                {
                    max = icheckprofile.Id;
                }

            }
            MainCounter = max + 1;
        }
        /// <summary>
        /// loop through the AIProffilesList and add each profile to the listview
        /// </summary>
        private void DisplayDefaultAI()
        {
            _lstAIPersons.Items.Clear();
            foreach (AIProfiles iprofile in AIProfileList)
            {
                ///////////////////////////////i don't believe that filling in the rest of AIProfiles will cus them to show up on screen throw this code ///////////////////////////
                //adding to the listview for display
                _lstAIPersons.Items.Add(iprofile);
            }
        }
        /// <summary>
        /// change the properties in the AIProfiles to the ones in the MainAICounter
        /// </summary>
        private void UpdateAIProfiles()
        {

            //making sure each AIProfiles is being given the appropriate property that the user selected
            for(int iprofile=0;iprofile < AIProfileList.Count; iprofile++)
            {
                ////////////////////////////////////////////
                //setting the AIProperty to the values in the MainAIContainer
                AIProfileList[iprofile].RiskTaker=(int)MainAIContainer[AllIds[AIProfileList[iprofile].Id.ToString()]]["Risk Taker"];
                AIProfileList[iprofile].BigBettor = (int)MainAIContainer[AllIds[AIProfileList[iprofile].Id.ToString()]]["Big Bettor"];
                AIProfileList[iprofile].ScaredyCat = (int)MainAIContainer[AllIds[AIProfileList[iprofile].Id.ToString()]]["ScaredyCat"];
                AIProfileList[iprofile].PlayingItSafe = (int)MainAIContainer[AllIds[AIProfileList[iprofile].Id.ToString()]]["Playing It Safe"];
                AIProfileList[iprofile].AtTable = (int)MainAIContainer[AllIds[AIProfileList[iprofile].Id.ToString()]]["At Table"];
                AIProfileList[iprofile].AIContainer = MainAIContainer;
                //if (iprofile == 0)
                //{
                    //AIProfileList[iprofile].AIContainer = MainAIContainer;
                //}
 
            }
        }
        /// <summary>
        /// change the game of the players in the game class it be identicle to the ones that was made here by the user
        /// </summary>
        private void Changenames()
        {
            //i need to loop throught the games playerList to do this so that is what i am doing
            foreach (Player iplayer in _game.PlayerList)
            {
                foreach (AIProfiles iAI in AIProfileList)
                {
                    if (iplayer.PlayerNum == iAI.AtTable)
                    {
                        iplayer.Name = iAI.Name;
                    }
                }
            }
        }
        /// <summary>
        /// takes in any trait and adds/replace the TraitHolder dictionary key and value with the parameters passed in through this method
        /// </summary>
        /// <param name="trait"></param>
        /// <param name="number"></param>
        private void ChoiceAnalysis(string trait, double number)
        {
            //for finding out how much elements in already in the TraitHolder
            int checkingif = TraitHolder.Count;

            int count = 0;

            bool keyInDict = false;
            if (TraitHolder.Count == 0)
            {
                
                TraitHolder.Add(trait, number);
            }
            else
            {
                //iterateing over the keys(i can get access to both the key and value by doing so now)
                foreach (string ikey in TraitHolder.Keys)
                {
                    if (ikey != trait)
                    {
                        //count will go up if there is no trait that has thesame name as a key in the current TraitHolder dictionary 
                        count = count + 1;

                    }
                    else if (ikey == trait)
                    {
                        keyInDict = true;

                    }
                }
                if (count == TraitHolder.Count)
                {    //count is used for dictionarys to get length
                    TraitHolder.Add(trait, number);
                }
                if (keyInDict == true)
                {
                    //remove the key value thats already in the dictionary and replace it with the new trait and it's value that the user choose
                    TraitHolder.Remove(trait);
                    TraitHolder.Add(trait, number);
                }
     
            }
        }
        /// <summary>
        /// adds a new _lstAIPersons items 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMakeNewAI(object sender, RoutedEventArgs e)
        {
            //clears away the items already in the _lstAIPersons
            TraitHolder.Clear();
            //make the deafult AIProfiles object and add it to the MainAIContainer and _lstAIPersons 
            AIProfiles newAI = new AIProfiles();
            MainCounterSavedValue();
            newAI.Name = "ArtificialInteligence" + MainCounter.ToString();
            CurrentNameHolder[0] = newAI.Name;
            newAI.RiskTaker = 0;
            TraitHolder.Add("Risk Taker", (double)newAI.RiskTaker);
            newAI.BigBettor = 0;
            TraitHolder.Add("Big Bettor", (double)newAI.BigBettor);
            newAI.ScaredyCat = 0;
            TraitHolder.Add("ScaredyCat", (double)newAI.ScaredyCat);
            newAI.PlayingItSafe = 0;
            TraitHolder.Add("Playing It Safe", (double)newAI.PlayingItSafe);
            newAI.AtTable = 0;
            TraitHolder.Add("At Table", (double)newAI.AtTable);

            MainAIContainer.Add(CurrentNameHolder[0], new Dictionary<string, double>());
            foreach (string ikey in TraitHolder.Keys)
            {
                MainAIContainer[CurrentNameHolder[0]].Add(ikey, TraitHolder[ikey]);
            }
            
            newAI.Id = MainCounter;
            ///////////////////// i need to check if the next value that main counter is going to already exist as a id///////////////////////////////
            ////////////is Allids id value deleted too in delete?////////////// what is this used for,anything crucial////////
            AllIds[newAI.Id.ToString()] = CurrentNameHolder[0];

            //increase the MainCounter by one for the next time so the next new created AI is increased by 1
            AIProfileList.Add(newAI);
            MainCounterSavedValue();

            _lstAIPersons.Items.Add(newAI);
            
            CurrentNameHolder[0] = null;

        }
        /// <summary>
        /// sets CurrentNameHolder[0] array to the choosen use name if its an appropriate name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onSelectName(object sender, RoutedEventArgs e)
        {
            int count = 0;
            // this the case for if the user enter a blank spaceing
            if (string.IsNullOrWhiteSpace(_txtInputName.Text))
            {
                
                MessageDialog msgDlg = new MessageDialog("That is not a name");
                msgDlg.ShowAsync();

            }
            else
            {
                foreach (AIProfiles ilookName in AIProfileList) {
                    if (ilookName.Name!= _txtInputName.Text)
                    {
                        count = count + 1;
                    }

                }
                if (count==AIProfileList.Count)
                {
                    //set the array for name to be the choosen user name
                    CurrentNameHolder[0] = _txtInputName.Text;
                }
                else 
                {


                }
            }


        }


        /// <summary>
        /// navigate to the GamePage when the back button is click by the user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGoBack(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), _game);
        }
        //cant change access modifiers when overriding
        /// <summary>
        /// called when navigating from the page,Saves all the progress,changes names then navigate back to the GamePage
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            //calling these methods here cus now is when the user has trul=y finished with their decision
            UpdateAIProfiles();
            Save();
            Changenames();    
        }


        /// <summary>
        /// receives the gameobject when this page is navigated to
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _game = e.Parameter as Game;
           
        }


    }
}
