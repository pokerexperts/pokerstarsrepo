﻿// Class: MainPage.xaml.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Code for the starting page of the application, with the three basic settings to alter the game.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace ROTPS3
{
    /// <summary>
    /// The main welcome/settings page where the user can make game adjustments before playing
    /// or going to the AI page to adjust the AI settings.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        /// <summary>
        /// The game object containing the logical layer of the application.
        /// </summary>
        private Game _game;

        /// <summary>
        /// The contructor of the main/setting page. Runs when the application starts.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            // Sets the game to null. It will be passed in or created via OnNavigatedTo().
            _game = null;
        }

        /// <summary>
        /// When the play button is pressed, the game will be passed to the GamePage as a transition happens.
        /// </summary>
        /// <param name="sender"> The play button. </param>
        /// <param name="e"> Arguments related to the play button. </param>
        private void OnPlay(object sender, RoutedEventArgs e)
        {
            // Check if the game file was imported and is to be resumed.
            // (Players will have null hands in a resumed file.)
            if ( _game.PlayerList[0].Hand == null )
            {
                // If the file has been imported, prepare it for play.
                _game.PrepareForPlay();
            }

            Frame.Navigate(typeof(GamePage), _game);
        }

        /// <summary>
        /// Saves any basic settings that have been changed in the three text boxes.
        /// </summary>
        /// <param name="sender"> The make changes button. </param>
        /// <param name="e"> Arguments related to the make changes button. </param>
        private void OnMakeChanges(object sender, RoutedEventArgs e)
        {
            // Save new player name if entered.
            if (!string.IsNullOrEmpty(_txtName.Text) && !string.IsNullOrWhiteSpace(_txtName.Text))
            {
                _game.PlayerList[0].Name = _txtName.Text;
            }

            // Save new starting money amount if entered.
            try
            {
                int newStartingMoney = int.Parse(_txtStartingMoney.Text);
                _game.StartingMoney = newStartingMoney;
                foreach(Player player in _game.PlayerList)
                {
                    player.ResetVals(newStartingMoney);
                }
            }
            catch(FormatException)
            {
                // Do nothing. Invalid entry, not an int.
            }
            
            // Save new minimum bet if entered.
            try
            {
                int newMinBet = int.Parse(_txtMinBet.Text);
                _game.MinBet = newMinBet;
            }
            catch (FormatException)
            {
                // Do nothing. Invalid entry, not an int.
            }
        }

        /// <summary>
        /// Sends the user to the AI page to adjust AI settings. This will pass the game object
        /// to the AI settings page.
        /// </summary>
        /// <param name="sender"> The Adjust AI button. </param>
        /// <param name="e"> Arguments related to the Adjust AI button. </param>
        private void OnAdjustAI(object sender, RoutedEventArgs e)
        {
            // Navigate to the AI page, sending the game object.
            Frame.Navigate(typeof(SettingsPage), _game);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != "")
            {
                // If the game is being restarted, use the game from the previous playthrough.
                // This will also accept the game object from the AI settings page.
                _game = e.Parameter as Game;
            }
            else
            {
                // Create a new game.
                _game = new Game();

                // Try to load a past game file.
                _game.GameJsonSerializer.Load();

                // If a game file was loaded, use that game object.
                if (_game.GameJsonSerializer.Game != null)
                {
                    _game = _game.GameJsonSerializer.Game;
                }
            }
        }
    }
}
