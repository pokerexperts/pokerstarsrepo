﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROTPS3
{
    class AI
    {
        /// <summary>
        /// for stroring the name of the AI
        /// </summary>
        private string _name;
        /// <summary>
        /// for storing the risktaking traits of the AI
        /// </summary>
        private int _riskTaker;
        /// <summary>
        /// 
        /// for storing the bigBettor traits of the AI
        /// </summary>
        private int _bigBettor;
        /// <summary>
        /// for storing the scaredycat traits of the AI
        /// </summary>
        private int _scaredyCat;
        /// <summary>
        /// for storing the playing it safe traits of the AI
        /// </summary>
        private int _playingItSafe;
        /// <summary>
        /// for storing the AT Table trait of the AI
        /// </summary>
        private int _atTable;
        /// <summary>
        /// for storing the player object that is passes through theis classes parameter
        /// </summary>
        private Player _player;
        /// <summary>
        /// stores a bool that shows if this AI has already raise previously
        /// </summary>
        private bool _hasRaised;

        /// <summary>
        /// stores the randomizer variable that is received from theAIManager class
        /// </summary>
        private Random _randomizer;
        /// <summary>
        /// store a string that tells us the rankings of the AI's hand
        /// </summary>
        private string _currentAIHand;
        /// <summary>
        /// represents a number from 0-100,the right of this number is going a decision to raise
        /// </summary>
        private double _rightRange;
        /// <summary>
        /// represents a number from 0-100,the left of this number is going a decision to fold
        /// </summary>
        private double _leftRange;
        /// <summary>
        /// stores the decision that the AI makes each turn
        /// </summary>
        private string _decision;
        /// <summary>
        /// stores the ammount the the AI will bet if they choose to raise
        /// </summary>
        private int _amountToBet;

        /// <summary>
        /// constructor for the AI that innitializes the randomizer variable
        /// </summary>
        public AI()
        {
            _randomizer = new Random();



        }
        /// <summary>
        /// get and set the feild variable that is of type string
        /// </summary>
        public string CurrentAIHand
        {
            get { return _currentAIHand; }
            set { _currentAIHand = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type Random
        /// </summary>
        public Random Randomizer
        {
            get { return _randomizer; }
            set { _randomizer = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type double
        /// </summary>
        public double RightRange
        {
            get { return _rightRange; }
            set { _rightRange = value;}
        }
        /// <summary>
        /// get and set the feild variable that is of type bool
        /// </summary>
        public bool HasRaised
        {
            get { return _hasRaised; }
            set { _hasRaised = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type Player
        /// </summary>
        public Player Player
        {
            get { return _player; }
            set { _player = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type double
        /// </summary>
        public double LeftRange
        {
            get { return _leftRange; }
            set { _leftRange = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type int
        /// </summary>
        public int AtTable
        {
            get { return _atTable; }
            set { _atTable = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type string
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        /// <summary>
        /// get and set a feild variable that is of type int
        /// </summary>
        public int RiskTaker
        {
            get { return _riskTaker; }
            set { _riskTaker = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type int
        /// </summary>
        public int BigBettor
        {
            get { return _bigBettor; }
            set { _bigBettor = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type int
        /// </summary>
        public int ScaredyCat
        {
            get { return _scaredyCat; }
            set { _scaredyCat = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type int
        /// </summary>
        public int PlayingItSafe
        {
            get { return _playingItSafe; }
            set { _playingItSafe = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type string
        /// </summary>
        public string Decision
        {
            get { return _decision; }
            set { _decision = value; }
        }
        /// <summary>
        /// get and set the feild variable that is of type int
        /// </summary>
        public int AmountToBet
        {
            get { return _amountToBet; }
            set { _amountToBet = value; }
        }
        /// <summary>
        /// checka the current AI's hand and calls the appropriate method based on it
        /// </summary>
        /// <param name="player"></param>
        /// <param name="tablecards"></param>
        /// <param name="randomizer"></param>
        public void MakeDecision(Player player, Table tablecards,Random randomizer)
        {
            Player = player;
            Randomizer = randomizer;
            //check if the table has cards revealed yet
            CardRank(player, tablecards);

            //call a method another method it this class to handle each particulater case of card hand
            if (CurrentAIHand=="A" || CurrentAIHand == "royal flush")
            {
                TopRankA();
            }
            else if (CurrentAIHand == "B" || CurrentAIHand == "straight flush")
            {
                TopRankB();
            }
            else if (CurrentAIHand == "C" || CurrentAIHand == "flush")
            {
                TopRankC();
            }
            else if (CurrentAIHand == "D" || CurrentAIHand == "full house")
            {
                TopRankD();
            }
            else if (CurrentAIHand == "E" || CurrentAIHand == "straight")
            {
                TopRankE();
            }
            else if (CurrentAIHand == "F" || CurrentAIHand == "four of a kind")
            {
                TopRankF();
            }
            else if (CurrentAIHand == "G" || CurrentAIHand == "three of a kind")
            {
                TopRankG();
            }
            else if (_currentAIHand == "H" || _currentAIHand == "two pair")
            {
                TopRankH();
            }
            else if (CurrentAIHand == "I" || CurrentAIHand == "pair")
            {
                TopRankI();
            }
            else
            {
                TopRankJ();
            }

        }
        /// <summary>
        /// sets the amount that will be betted, and sets the _hasRaised variable
        /// </summary>
        public void newBet()
        {

            //Random rand = new Random();
            int theRand = Randomizer.Next(50, 100);
            double percentAmount = Player.Money * (BigBettor + 1) * 0.01;
            theRand += (int)percentAmount;
            //with theses two variables set the AIManager can just access them for passing the game class's method
            AmountToBet = theRand;
            HasRaised = true;

        }
        /// <summary>
        /// increases the chance of calling normal round
        /// </summary>
        private void PlayingItSafeActivated()
        {
            double foldNRaiseGrouped = 100 - (RightRange - LeftRange);
            double foldPercentageGrouped = LeftRange / foldNRaiseGrouped;
            double raisePercentageGrouped = (100 - RightRange) / foldNRaiseGrouped;
            RightRange = RightRange + PlayingItSafe;
            double newFoldNRaiseGrouped = 100 - RightRange;
            double newFoldGrouped = newFoldNRaiseGrouped * foldPercentageGrouped;
            //double newRaiseGrouped = newFoldNRaiseGrouped * raisePercentageGrouped;
            LeftRange = newFoldGrouped;
        }
        /// <summary>
        /// increases the chance of folding at a higher round
        /// </summary>
        private void ScaredyCatActivated()
        {
            double callNRaiseGrouped = 100 - LeftRange;
            double callPercentageGrouped = (RightRange - LeftRange) / callNRaiseGrouped;
            double raisePercentageGrouped = (100 - RightRange) / callNRaiseGrouped;
            LeftRange = LeftRange + ScaredyCat;
            double newCallNRaiseGrouped = 100 - LeftRange;
            //double newCallGrouped = newCallNRaiseGrouped * callPercentageGrouped;
            double newRaiseGrouped = newCallNRaiseGrouped * raisePercentageGrouped;
            RightRange = 100 - newRaiseGrouped;

        }
        /// <summary>
        /// increases the chance of raising at a low hand round
        /// </summary>
        private void RiskTakerActivated()
        {
            double foldNCallGrouped = RightRange;
            double foldPercentageGrouped = LeftRange / foldNCallGrouped;
            double callPercentageGrouped = (RightRange - LeftRange) / foldNCallGrouped;
            RightRange = RightRange - RiskTaker;
            double newfoldNCallGrouped = 100 - RightRange;
            RightRange = newfoldNCallGrouped;
            double newfoldGrouped = newfoldNCallGrouped * foldPercentageGrouped;
            LeftRange = newfoldGrouped;
            //double newcallGrouped = newfoldNCallGrouped * callPercentageGrouped;
        }
        /// <summary>
        /// Makes the AI decision based on the properties of the AI itself 
        /// also it contains specific methods calls for the card hand that is ranked "A"
        /// </summary>
        public void TopRankA()
        {
            //default left and right range values for this method
            RightRange = 35;
            LeftRange = 25;
            ScaredyCatActivated();
            int guessNumber=Randomizer.Next(1,101);

            if (guessNumber>LeftRange && HasRaised==true)
            {
                Decision = "Call";
            }
            else if (guessNumber >= RightRange)
            {
                Decision = "Raise";
                newBet();
                
            }
            else if (guessNumber>LeftRange && guessNumber < RightRange)
            {
                //call
                Decision = "Call";
            }
            else{
                //fold
                Decision = "Fold";
            }
  
        }
        /// <summary>
        ///Makes the AI decision based on the properties of the AI itself 
        /// also it contains specific methods calls for the card hand that is ranked "B"
        /// </summary>
        public void TopRankB()
        {
            //default left and right range values for this method
            RightRange = 38;
            LeftRange = 22;
            ScaredyCatActivated();
            int guessNumber = Randomizer.Next(1, 101);

            if (guessNumber > LeftRange && HasRaised == true)
            {
                Decision = "Call";
            }
            else if (guessNumber >= RightRange)
            {
                //raise
                Decision = "Raise";
                newBet();
                
            }
            else if (guessNumber > LeftRange && guessNumber < RightRange)
            {
                //call
                Decision = "Call";
            }
            else
            {
                //fold
                Decision = "Fold";
            }
        }
        /// <summary>
        ///Makes the AI decision based on the properties of the AI itself 
        /// also it contains specific methods calls for the card hand that is ranked "C"
        /// </summary>
        public void TopRankC()
        {
            //default left and right range values for this method
            RightRange = 40;
            LeftRange = 20;
            ScaredyCatActivated();
            int guessNumber = Randomizer.Next(1, 101);

            if (guessNumber > LeftRange && HasRaised == true)
            {
                Decision = "Call";
            }
            else if (guessNumber >= RightRange)
            {
                //raise
                Decision = "Raise";
                newBet();
                
            }
            else if (guessNumber > LeftRange && guessNumber < RightRange)
            {
                //call
                Decision = "Call";
            }
            else
            {
                //fold
                Decision = "Fold";
            }
        }
        /// <summary>
        /// Makes the AI decision based on the properties of the AI itself 
        /// also it contains specific methods calls for the card hand that is ranked "D"
        /// </summary>
        public void TopRankD()
        {
            //default left and right range values for this method
            RightRange = 50;
            LeftRange = 18;
            ScaredyCatActivated();
            PlayingItSafeActivated();
            int guessNumber = Randomizer.Next(1, 101);


            if (guessNumber > LeftRange && HasRaised == true)
            {
                Decision = "Call";
            }
            else if (guessNumber >= RightRange)
            {
                //raise
                Decision = "Raise";
                newBet();
                
            }
            else if (guessNumber > LeftRange && guessNumber < RightRange)
            {
                //call
                Decision = "Call";
            }
            else
            {
                //fold
                Decision = "Fold";
            }
        }
        /// <summary>
        /// Makes the AI decision based on the properties of the AI itself 
        /// also it contains specific methods calls for the card hand that is ranked "E"
        /// </summary>
        public void TopRankE()
        {
            //default left and right range values for this method
            RightRange = 55;
            LeftRange = 15;
            ScaredyCatActivated();
            PlayingItSafeActivated();

            int guessNumber = Randomizer.Next(1, 101);

            if (guessNumber > LeftRange && HasRaised == true)
            {
                Decision = "Call";
            }
            else if (guessNumber >= RightRange)
            {
                //raise
                Decision = "Raise";
                newBet();
                
            }
            else if (guessNumber > LeftRange && guessNumber < RightRange)
            {
                //call
                Decision = "Call";
            }
            else
            {
                //fold
                Decision = "Fold";
            }
        }
        /// <summary>
        /// Makes the AI decision based on the properties of the AI itself 
        /// also it contains specific methods calls for the card hand that is ranked "F"
        /// </summary>
        public void TopRankF()
        {
            //default left and right range values for this method
            RightRange = 73;
            LeftRange = 30;
            PlayingItSafeActivated();
            int guessNumber = Randomizer.Next(1, 101);

            if (guessNumber > LeftRange && HasRaised == true)
            {
                Decision = "Call";
            }
            else if (guessNumber >= RightRange)
            {
                //raise
                Decision = "Raise";
                newBet();
                
            }
            else if (guessNumber > LeftRange && guessNumber < RightRange)
            {
                //call
                Decision = "Call";
            }
            else
            {
                //fold
                Decision = "Fold";
            }
        }
        /// <summary>
        /// Makes the AI decision based on the properties of the AI itself 
        /// also it contains specific methods calls for the card hand that is ranked "G"
        /// </summary>
        public void TopRankG()
        {
            //default left and right range values for this method
            RightRange = 69;
            LeftRange = 38;
            RiskTakerActivated();
            int guessNumber = Randomizer.Next(1, 101);


            if (guessNumber > LeftRange && HasRaised == true)
            {
                Decision = "Call";
            }
            if (guessNumber >= RightRange)
            {
                //raise
                Decision = "Raise";
                newBet();
                
            }
            else if (guessNumber > LeftRange && guessNumber < RightRange)
            {
                //call
                Decision = "Call";
            }
            else
            {
                //fold
                Decision = "Fold";
            }
        }
        /// <summary>
        /// Makes the AI decision based on the properties of the AI itself 
        /// also it contains specific methods calls for the card hand that is ranked "H"
        /// </summary>
        public void TopRankH()
        {
            //default left and right range values for this method
            RightRange = 75;
            LeftRange = 42;
            RiskTakerActivated();

            int guessNumber = Randomizer.Next(1, 101);

            if (guessNumber > LeftRange && HasRaised == true)
            {
                Decision = "Call";
            }
            else if (guessNumber >= RightRange)
            {
                //raise
                Decision = "Raise";
                newBet();
                
            }
            else if (guessNumber > LeftRange && guessNumber < RightRange)
            {
                //call
                Decision = "Call";
            }
            else
            {
                //fold
                Decision = "Fold";
            }
        }
        /// <summary>
        /// Makes the AI decision based on the properties of the AI itself 
        /// also it contains specific methods calls for the card hand that is ranked "I"
        /// </summary>
        public void TopRankI()
        {
            //default left and right range values for this method
            RightRange = 72;
            LeftRange = 45;
            RiskTakerActivated();
            int guessNumber = Randomizer.Next(1, 101);

            if (guessNumber > LeftRange && HasRaised == true)
            {
                Decision = "Call";
            }
            else if (guessNumber >= RightRange)
            {
                //raise
                Decision = "Raise";
                newBet();
            }
            else if (guessNumber > LeftRange && guessNumber < RightRange)
            {
                //call
                Decision = "Call";
                
            }
            else
            {
                //fold
                Decision = "Fold";
            }
        }
        /// <summary>
        /// Makes the AI decision based on the properties of the AI itself 
        /// also it contains specific methods calls for the card hand that is ranked "J"
        /// </summary>
        public void TopRankJ()
        {
            //default left and right range values for this method
            RightRange = 70;
            LeftRange = 60;
            RiskTakerActivated();
            int guessNumber = Randomizer.Next(1, 101);

            if (guessNumber > LeftRange && HasRaised == true)
            {
                Decision = "Call";
            }
            else if (guessNumber >= RightRange)
            {
                //raise
                Decision = "Raise";
                newBet();
                
            }
            else if (guessNumber > LeftRange && guessNumber < RightRange)
            {
                //call
                Decision = "Call";
            }
            else
            {
                //fold
                Decision = "Fold";
            }
        }
        /// <summary>
        /// Gives the games in the AI's hand a rank so its easy for use in this class
        /// </summary>
        /// <param name="player"></param>
        /// <param name="tablecards"></param>
        public void CardRank(Player player, Table tablecards)
        {
            if (tablecards.RevealedCardList == null || tablecards.RevealedCardList.Count == 0)
            {
                if (player.Eliminated == false)
                {
                    //check what 2 cards they got
                    if (player.Hand.HandList[0].Value == 14 || player.Hand.HandList[1].Value == 14)
                    {
                        _currentAIHand = "A";
                    }
                    else if (player.Hand.HandList[0].Value == 13 || player.Hand.HandList[1].Value == 13)
                    {
                        _currentAIHand = "B";
                    }
                    else if (player.Hand.HandList[0].Value == 12 || player.Hand.HandList[1].Value == 12)
                    {
                        _currentAIHand = "C";
                    }
                    else if (player.Hand.HandList[0].Value == 11 || player.Hand.HandList[1].Value == 11)
                    {
                        _currentAIHand = "D";
                    }
                    else if (player.Hand.HandList[0].Value == 10 || player.Hand.HandList[1].Value == 10)
                    {
                        _currentAIHand = "E";
                    }
                    else if (player.Hand.HandList[0].Value == 9 || player.Hand.HandList[1].Value == 9)
                    {
                        _currentAIHand = "F";
                    }
                    else if (player.Hand.HandList[0].Value == 8 || player.Hand.HandList[1].Value == 8)
                    {
                        _currentAIHand = "G";
                    }
                    else if (player.Hand.HandList[0].Value == 7 || player.Hand.HandList[1].Value == 7)
                    {
                        _currentAIHand = "H";
                    }
                    else if (player.Hand.HandList[0].Value == 6 || player.Hand.HandList[1].Value == 6)
                    {
                        _currentAIHand = "I";
                    }
                    else
                    {
                        _currentAIHand = "J";
                    }
                }
            }
            else
            {
                //check what their best card is
                player.Hand.HandScore(tablecards.RevealedCardList);
                _currentAIHand = player.Hand.HandType;
            }
        }

    }
}
