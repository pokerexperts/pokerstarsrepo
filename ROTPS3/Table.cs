﻿// Class: Table.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Used to simulate the table for the cards to be stored in and displayed on during the game.


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace ROTPS3
{
    /// <summary>
    /// The cards on the table. These are public, used in anyone's hand.
    /// </summary>
    class Table
    {
        /// <summary>
        /// The list of cards to be revealed on the table and used publically.
        /// </summary>
        private List<Card> _revealedCardList;

        /// <summary>
        /// The image control for the first card from the left on the table.
        /// </summary>
        private Image _imgFirstCard;

        /// <summary>
        /// The image control for the second card from the left on the table.
        /// </summary>
        private Image _imgSecondCard;

        /// <summary>
        /// The image control for the third card from the left on the table.
        /// </summary>
        private Image _imgThirdCard;

        /// <summary>
        /// The image control for the fourth card from the left on the table.
        /// </summary>
        private Image _imgFourthCard;

        /// <summary>
        /// The image control for the fifth card from the left on the table.
        /// </summary>
        private Image _imgFifthCard;

        /// <summary>
        /// The constructor for the table. Creates an empty card list for the revealed cards to populate.
        /// Also sets image controls for revealing cards on the table.
        /// </summary>
        /// <param name="cardImage1"> The image control for the first card on the table from the left. </param>
        /// <param name="cardImage2"> The image control for the second card on the table from the left. </param>
        /// <param name="cardImage3"> The image control for the third card on the table from the left. </param>
        /// <param name="cardImage4"> The image control for the fourth card on the table from the left. </param>
        /// <param name="cardImage5"> The image control for the fifth card on the table from the left. </param>
        public Table(Image cardImage1, Image cardImage2, Image cardImage3, Image cardImage4, Image cardImage5)
        {
            // Set the image controls to be used in revealing cards.
            _imgFirstCard = cardImage1;
            _imgSecondCard = cardImage2;
            _imgThirdCard = cardImage3;
            _imgFourthCard = cardImage4;
            _imgFifthCard = cardImage5;


            // Initializes the card list.
            RevealedCardList = new List<Card>();
        }

        /// <summary>
        /// The first card on the table.
        /// </summary>
        public Image FirstCard
        {
            get { return _imgFirstCard; }
            set { _imgFirstCard = value; }
        }

        /// <summary>
        /// The second card on the table.
        /// </summary>
        public Image SecondCard
        {
            get { return _imgSecondCard; }
            set { _imgSecondCard = value; }
        }

        /// <summary>
        /// The third card on the table.
        /// </summary>
        public Image ThirdCard
        {
            get { return _imgThirdCard; }
            set { _imgThirdCard = value; }
        }

        /// <summary>
        /// The fourth card on the table.
        /// </summary>
        public Image FourthCard
        {
            get { return _imgFourthCard; }
            set { _imgFourthCard = value; }
        }

        /// <summary>
        /// The fifth card on the table.
        /// </summary>
        public Image FifthCard
        {
            get { return _imgFifthCard; }
            set { _imgFifthCard = value; }
        }

        /// <summary>
        /// The list of revealed cards. This list is public to all players' use.
        /// </summary>
        public List<Card> RevealedCardList
        {
            get
            {
                return _revealedCardList;
            }

            set
            {
                _revealedCardList = value;
            }
        }
    }
}
