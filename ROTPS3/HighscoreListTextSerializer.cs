﻿// Class: HighscoreListTextSerializer.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Used to save highscore list in a text file on the device.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace ROTPS3
{
    /// <summary>
    /// Data persistence class made to save highscores to a text file.
    /// </summary>
    class HighscoreListTextSerializer
    {
        /// <summary>
        /// The stored highscore list from the game.
        /// </summary>
        protected List<Score> _highscoreList;

        /// <summary>
        /// The highscore list save directory.
        /// </summary>
        protected string _highscoreSavePath;

        /// <summary>
        /// Constructor for the highscore text-file serializer. Initializes highscore list as null, to be
        /// set as a property later on.
        /// </summary>
        public HighscoreListTextSerializer()
        {
            // Initialize the highscore list as null.
            _highscoreList = new List<Score>();

            // Initialize the save path for the highscore list file.
            _highscoreSavePath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "HighscoreListFolder");

            // Create a directory for the highscore file if it does not exist.
            if (Directory.Exists(_highscoreSavePath) == false)
            {
                Directory.CreateDirectory(_highscoreSavePath);
            }
        }

        /// <summary>
        /// The stored highscore list from the game.
        /// </summary>
        public List<Score> HighscoreList
        {
            get { return _highscoreList; }
            set { _highscoreList = value; }
        }

        /// <summary>
        /// Load the highscore list file.
        /// </summary>
        public void Load()
        {
            // Determine the file name for the highscore list save file.
            string saveFilePath = $"{_highscoreSavePath}/HighscoreSaveFile.dat";

            // Check if the save file exists.
            if (File.Exists(saveFilePath) == true)
            {
                // Load the highscore list from a save file if it exists, then store it for the game.
                using (StreamReader highscoreLoader = new StreamReader(new FileStream(saveFilePath, FileMode.Open)))
                {
                    // Check for ten highscores in the file, saving them to the highscore list.
                    for(int highscoreNum = 0; highscoreNum < 10; highscoreNum++)
                    {
                        string name = highscoreLoader.ReadLine();
                        int score = int.Parse(highscoreLoader.ReadLine());
                        Score importedScore = new Score(name, score);
                        _highscoreList.Add(importedScore);
                    }
                }
            }
        }

        /// <summary>
        /// Save the highscore list.
        /// </summary>
        public void Save()
        {
            // Determine the file name for the highscore list save file.
            string saveFilePath = $"{_highscoreSavePath}/HighscoreSaveFile.dat";

            // Create the highscore list file and save the list to it from the game.
            using (StreamWriter highscoreSaver = new StreamWriter(new FileStream(saveFilePath, FileMode.Create)))
            {
                // For each highscore in the list, write the player's name and then their score to the file.
                foreach(Score score in _highscoreList)
                {
                    highscoreSaver.WriteLine($"{score.Name}");
                    highscoreSaver.WriteLine($"{score.ScoreValue}");
                }
            }
        }
    }
}
