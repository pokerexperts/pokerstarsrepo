﻿// Class: Deck.cs
// Author: Brigham Moll
// Date: April 2017
// Part of Revenge of the Poker Stars 3. Created at Sheridan College.
// Used to store cards in the deck being used during the course of a game, as well as retrieve them.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROTPS3
{
    /// <summary>
    /// This class represents the deck of cards used to play the game.
    /// </summary>
    class Deck
    {

        /// <summary>
        /// The list of cards in the deck.
        /// </summary>
        private List<Card> _deckList;

        /// <summary>
        /// The contructor for the deck of cards. This will create and shuffle the cards for the deck.
        /// </summary>
        public Deck()
        {
            // Create and shuffle the cards.
            CreateCards();
            ShuffleCards();            
        }

        /// <summary>
        /// The list of cards in the deck.
        /// </summary>
        public List<Card> DeckList
        {
            get { return _deckList; }
            set { _deckList = value; }
        }

        /// <summary>
        ///  Creates a complete deck with all the cards of every suit.
        /// </summary>
        public void CreateCards()
        {
            // Intialize the deckList to put cards into.
            _deckList = new List<Card>();


            // Go through every suit in the deck to create cards for that suit.
            foreach (Suit iSuits in Enum.GetValues(typeof(Suit)))
            {
                // Go through every card value in the deck (Using values 2-14, Ace being 14.)
                for (int value = 2; value <= 14; value++)
                {
                    // Create a card.
                    Card card = new Card();
                    // Set a value for the card.
                    card.Value = value;
                    // Set a suit for the card.
                    card.Suit = iSuits;
                    // Store the string used to find the card's image.
                    card.GetImageLocation();
                    // Add card created to decklist.
                    _deckList.Add(card);
                }

            }
        }

        /// <summary>
        /// This method shuffles the cards in the deck.
        /// </summary>
        public void ShuffleCards()
        {
            // The cards are shuffled using a shuffling algorithm.
            int iCard = _deckList.Count();
            Random rnd = new Random();
            while (iCard > 1)
            {
                int rndCard = (rnd.Next(0, iCard));
                iCard--;

                Card value = _deckList[rndCard];
                _deckList[rndCard] = _deckList[iCard];
                _deckList[iCard] = value;
                
            }

        
        }

        /// <summary>
        /// Draws a card from the deck.
        /// </summary>
        /// <returns> Returns the card object drawn out of the deck. </returns>
        public Card DrawCard()
        {
            // Takes a card off of the top of the deck and returns it.
            Card cardTop = _deckList[0];
            _deckList.RemoveAt(0);
            return cardTop;
        }


    }
}
